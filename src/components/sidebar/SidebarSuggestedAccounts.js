import React from "react";
import userNoPict from "../../assets/images/user_no-pict.jpg";
import ContentLoader from "react-content-loader";

export const SuggestedAccounts = ({ randomusers, props }) => {
  //
  //debug
  console.log("suggested_account_numbers:", randomusers);

  if (randomusers === null || randomusers === undefined) {
    return (
      <ContentLoader
        speed={2}
        width={280}
        height={200}
        viewBox="0 0 280 200"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
      >
        <rect x="40" y="10" rx="3" ry="3" width="140" height="6" />
        <rect x="40" y="23" rx="3" ry="3" width="90" height="6" />
        <circle cx="20" cy="20" r="15" />

        <rect x="40" y="60" rx="3" ry="3" width="140" height="6" />
        <rect x="40" y="73" rx="3" ry="3" width="90" height="6" />
        <circle cx="20" cy="70" r="15" />

        <rect x="40" y="110" rx="3" ry="3" width="140" height="6" />
        <rect x="40" y="123" rx="3" ry="3" width="90" height="6" />
        <circle cx="20" cy="120" r="15" />

        <rect x="40" y="160" rx="3" ry="3" width="140" height="6" />
        <rect x="40" y="173" rx="3" ry="3" width="90" height="6" />
        <circle cx="20" cy="170" r="15" />
      </ContentLoader>
    );
  }

  const AccountSuggestedCard = (publicuser) => {
    const userAva = !publicuser.avatar ? userNoPict : publicuser.avatar;

    const Fullname =
      publicuser.firstname === "" || publicuser.firstname === null
        ? publicuser.username + publicuser.user_id
        : `${publicuser.firstname} ${publicuser.lastname}`;

    return (
      <div className="suggested-account-item mt-4">
        <div className="image-profile-box">
          <div className="image-profile">
            <img
              className="avatar-small img-src"
              src={userAva}
              alt={
                publicuser.firstname + publicuser.lastname + "Picture Profile"
              }
            />
          </div>
        </div>
        <div className="username-follow-wrapper justify-content-between">
          <div className="bio-desc pl-2">
            <a className="suggested-user-link" href={"/" + publicuser.username}>
              <div className="username">{publicuser.username}</div>
              <div className="name">{Fullname}</div>
            </a>
          </div>
          {/* <div className="d-block">
            <button href="#" className="suggested-account-folow-btn btn">
              Follow
            </button>
          </div> */}
        </div>
      </div>
    );
  };

  const AccountSuggestedCards = randomusers.map((publicuser) =>
    AccountSuggestedCard(publicuser)
  );

  return <div className="suggested-users-list">{AccountSuggestedCards}</div>;
};
