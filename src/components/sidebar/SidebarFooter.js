import React from "react";

export const InfoWeb = ({ linksInfoWeb }) => {
  if (linksInfoWeb.length === 0) return null;
  //
  //
  //debug
  // console.log(
  //   "suggested_account_numbers:",
  //   linksInfoWeb.length,
  //   linksInfoWeb.id
  // );

  const listInfoWeb = (linkInfoWeb) => {
    //
    //debug
    // console.log(linkInfoWeb.id);
    return (
      <li className="list-link" key={linkInfoWeb}>
        <a
          className={"list-link-" + linkInfoWeb}
          href={"/" + linkInfoWeb}
          target="_blank"
          rel="noreferrer"
          key={linkInfoWeb}
        >
          {linkInfoWeb}
        </a>
      </li>
    );
  };

  const listsInfoWeb = linksInfoWeb.map((linkInfoWeb) =>
    listInfoWeb(linkInfoWeb)
  );

  return (
    <div className="info-web">
      <div className="about">
        <nav className="nav-about">
          <ul className="underlist-link">{listsInfoWeb}</ul>
        </nav>
        <span className="copyright">© 2019 SOSMET FROM FSWWAVETWO</span>
      </div>
    </div>
  );
};
