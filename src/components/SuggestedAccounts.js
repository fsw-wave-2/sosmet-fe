import React from "react";
import userNoPict from "../assets/images/user_no-pict.jpg";

export const SuggestedAccounts = ({ publicusers }) => {
  if (publicusers.length === 0) return null;
  //
  //debug
  // console.log("suggested_account_numbers:", publicusers.length);

  const AccountSuggestedCard = (publicuser) => {
    const userAva = !publicuser.avatar ? userNoPict : publicuser.avatar;

    const Fullname =
      publicuser.firstname === "" || publicuser.firstname === null
        ? publicuser.username + publicuser.user_id
        : `${publicuser.firstname} ${publicuser.lastname}`;

    return (
      <div className="suggested-account-item mt-4">
        <div className="image-profile-box">
          <div className="image-profile">
            <img
              className="avatar-small img-src"
              src={userAva}
              alt={
                publicuser.firstname + publicuser.lastname + "Picture Profile"
              }
            />
          </div>
        </div>
        <div className="username-follow-wrapper justify-content-between">
          <div className="bio-desc pl-2">
            <a className="suggested-user-link" href={"/" + publicuser.username}>
              <div className="username">{publicuser.username}</div>
              <div className="name">{Fullname}</div>
            </a>
          </div>
          {/* <div className="d-block">
            <button href="#" className="suggested-account-folow-btn btn">
              Follow
            </button>
          </div> */}
        </div>
      </div>
    );
  };

  const AccountSuggestedCards = publicusers.map((publicuser) =>
    AccountSuggestedCard(publicuser)
  );

  return <div className="suggested-users-list">{AccountSuggestedCards}</div>;
};
