import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../redux/actions/authAction";
import classnames from "classnames";

class Register extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      password2: "",
      errors: {},
      errorPassword: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    // if(nextProps.auth.isAuthenticated){
    //     // to push user into dashboard screen after login
    //     this.props.history.push("/dashboard")
    // }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
    if (this.state.password !== this.state.password2) {
      this.setState({ errorPassword: " unmatched" });
    }
  };

  onSubmit = (e) => {
    e.preventDefault();

    const newUser = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    };

    // console.log(newUser);
    this.props.registerUser(newUser, this.props.history);
  };

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect to dashboard
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/home");
    }
  }

  render() {
    const { errors } = this.state;
    // console.log("states", this.state);
    return (
      <div className="register-page">
        <div className="container">
          <div
            className="row justify-content-center"
            style={{ marginTop: "16vh" }}
          >
            <div className="col-8 col-lg-8 col-md-6 col-sm-4">
              <div className="text-center">
                <h4>
                  <b>Register</b> below
                </h4>
              </div>
              <div className="row justify-content-center">
                <form
                  noValidate
                  onSubmit={this.onSubmit}
                  className="justify-content-center col-5 col-lg-5 col-md-4 col-sm-4 mt-5 mb-5"
                >
                  <div className="mb-3">
                    <label class="form-label">Username</label>
                    <input
                      onChange={this.onChange}
                      value={this.state.username}
                      error={errors.username}
                      id="username"
                      autoComplete="off"
                      type="text"
                      className={classnames("auth-form-control form-control", {
                        invalid: errors.username,
                      })}
                    />
                    <span className="red-text">{errors.username}</span>
                  </div>
                  <div className="mb-3">
                    <label for="email" class="form-label">
                      Email
                    </label>
                    <input
                      onChange={this.onChange}
                      value={this.state.email}
                      error={errors.email}
                      autoComplete="off"
                      id="email"
                      type="text"
                      className={classnames("auth-form-control form-control", {
                        invalid: errors.email,
                      })}
                    />
                    <span className="red-text">{errors.email}</span>
                  </div>
                  <div className="mb-3">
                    <label for="password" class="form-label">
                      Password
                    </label>
                    <input
                      onChange={this.onChange}
                      value={this.state.password}
                      error={errors.password}
                      id="password"
                      autoComplete="off"
                      type="password"
                      className={classnames("auth-form-control form-control", {
                        invalid: errors.password,
                      })}
                    />
                    <span className="red-text">{errors.password}</span>
                  </div>
                  <div className="mb-3">
                    <label for="password2" class="form-label">
                      Password
                    </label>
                    <input
                      onChange={this.onChange}
                      value={this.state.password2}
                      error={errors.password}
                      autoComplete="off"
                      id="password2"
                      type="password"
                      className={classnames("auth-form-control form-control", {
                        invalid: errors.password,
                      })}
                    />
                    {this.state.password2 &&
                    this.state.password !== this.state.password2 ? (
                      <p
                        style={{
                          color: "red",
                          fontSize: "11px",
                          paddingTop: "8px",
                          paddingBottom: "8px",
                        }}
                      >
                        {this.state.errorPassword}
                      </p>
                    ) : (
                      <p></p>
                    )}
                    <span className="red-text">{errors.password}</span>
                  </div>
                  <div className="text-center mt-5">
                    <span className="text-danger">{errors.message}</span>
                  </div>
                  <div className="text-center mt-5 f-12">
                    <button type="submit" className="btn btn-purple">
                      Sign Up
                    </button>
                  </div>
                </form>
              </div>
              <div className="text-center my-3">
                <span>
                  Have an account? Let's{" "}
                  <Link to="/accounts/login">Login!</Link>
                </span>
              </div>
            </div>
          </div>
          <div className="back-to-home-auth position-absolute">
            <Link
              to="/"
              className="btn text-purple text-decoration-none d-flex link-homes col-12"
            >
              <div style={{ fontSize: "28px" }} className="p-0">
                <span>
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M20 8.00001L14 2.74001C13.45 2.24805 12.7379 1.97607 12 1.97607C11.262 1.97607 10.55 2.24805 9.99997 2.74001L3.99997 8.00001C3.68234 8.28408 3.42887 8.63256 3.25644 9.02225C3.08402 9.41194 2.99659 9.83389 2.99997 10.26V19C2.99997 19.7957 3.31604 20.5587 3.87865 21.1213C4.44126 21.6839 5.20432 22 5.99997 22H18C18.7956 22 19.5587 21.6839 20.1213 21.1213C20.6839 20.5587 21 19.7957 21 19V10.25C21.0019 9.82557 20.9138 9.40555 20.7414 9.01769C20.5691 8.62983 20.3163 8.28296 20 8.00001ZM14 20H9.99997V15C9.99997 14.7348 10.1053 14.4804 10.2929 14.2929C10.4804 14.1054 10.7348 14 11 14H13C13.2652 14 13.5195 14.1054 13.7071 14.2929C13.8946 14.4804 14 14.7348 14 15V20ZM19 19C19 19.2652 18.8946 19.5196 18.7071 19.7071C18.5195 19.8946 18.2652 20 18 20H16V15C16 14.2044 15.6839 13.4413 15.1213 12.8787C14.5587 12.3161 13.7956 12 13 12H11C10.2043 12 9.44126 12.3161 8.87865 12.8787C8.31604 13.4413 7.99997 14.2044 7.99997 15V20H5.99997C5.73476 20 5.4804 19.8946 5.29287 19.7071C5.10533 19.5196 4.99997 19.2652 4.99997 19V10.25C5.00015 10.108 5.03057 9.9677 5.08919 9.83839C5.14781 9.70907 5.2333 9.59372 5.33997 9.50001L11.34 4.25001C11.5225 4.08969 11.7571 4.00127 12 4.00127C12.2429 4.00127 12.4775 4.08969 12.66 4.25001L18.66 9.50001C18.7666 9.59372 18.8521 9.70907 18.9108 9.83839C18.9694 9.9677 18.9998 10.108 19 10.25V19Z"
                      fill="purple"
                    />
                  </svg>
                </span>
              </div>
              <div className="font-weight-regular pt-3 pr-3 pb-3 pl-2 f-10">
                back to home
              </div>
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth, //recieve state from authreducer
  errors: state.errors, // recieve state from errorreducer
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));
