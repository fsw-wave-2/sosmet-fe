import React, { useState } from "react";
import { useParams, Link, Switch, Route, useLocation } from "react-router-dom";
import ContentLoader from "react-content-loader";
import { Row } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { fetchPostsUserId } from "../../redux/actions/fetchPostsUserIdAction";
import { UserPublicPost } from "../post/UserPosts";
import { Button } from "react-bootstrap";
import { JournalsPublic } from "../UserJournalsPublic";
import { fetchPublicJournals } from "../../redux/actions/fetchPublicJournalsUserAction";
import userNoPict from "../../assets/images/user_no-pict.jpg";
import NewlineText from "../profile/BioNewLineText";

const PublicUserDetail = ({
  searchuser,
  props,
  whoami,
  handleShow,
  matchwhoami,
  onClick,
  auth,
  bio,
}) => {
  let { us } = useParams();
  let match = useLocation();
  const dispatch = useDispatch();
  const loading = useSelector((state) => state.postsuserid.loading);
  const postsuserid = useSelector((state) => state.postsuserid.postsuserid);
  const publicjournalsuser = useSelector(
    (state) => state.fetchpublicjournals.fetchpublicjournals
  );
  const loadingPublicJournals = useSelector(
    (state) => state.fetchpublicjournals.loading
  );
  const [posts, setPosts] = useState(true);
  const [journals, setJournals] = useState();

  //
  //debug
  // console.log("images:", images);
  // console.log("setImages:", setImages);
  console.log("us:", us);
  console.log("match:", match);
  // console.log("loading:", loading);
  // console.log("postsuserid:", postsuserid);
  // console.log("matchwhoami:", matchwhoami);
  // console.log("whoami:", whoami.username);
  // console.log("publicjournalsuser:", publicjournalsuser);
  // console.log("loadingPublicJournals:", loadingPublicJournals);
  // console.log("postuserid:", postsuserid.postsuserid.length);
  // console.log("props:", props);
  // console.log("searchuser", searchuser);

  if (searchuser === undefined) {
    return (
      <div className="mmm">
        <ContentLoader
          speed={2}
          width={900}
          height={260}
          viewBox="0 0 900 260"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="470" y="26" rx="3" ry="3" width="140" height="20" />
          <rect x="470" y="62" rx="3" ry="3" width="300" height="14" />
          <rect x="470" y="87" rx="3" ry="3" width="300" height="14" />
          <rect x="470" y="112" rx="3" ry="3" width="300" height="14" />
          <rect x="470" y="137" rx="3" ry="3" width="200" height="14" />
          <circle cx="366" cy="80" r="70" />
        </ContentLoader>
      </div>
    );
  }

  if (loading === true) {
    dispatch(fetchPostsUserId(searchuser.user_id));
    dispatch(fetchPublicJournals(searchuser.user_id));
  }

  // const postsUserIdData = fetchPostsUserId(searchuser.user_id);
  //
  //debug
  // console.log("postsUserIdData:", postsUserIdData);

  // console.log("count:", count);

  // dispatch(fetchPostsUserId(searchuser.user_id));

  const userAva = !searchuser.avatar ? userNoPict : searchuser.avatar;

  return (
    <>
      <Row className="justify-content-center pub-page-first-row">
        <div className="col-lg-2 mt-1">
          <img
            src={userAva}
            style={{ borderRadius: "100%" }}
            height="160px"
            className="avatar-large"
            alt={`Profile of ${searchuser.firstname} ${searchuser.lastname}`}
          />
        </div>
        <div className="col-lg-4 align-items-center position-relative">
          <div className="second-col-1st-row row  mt-4">
            <div className="second-col-1st-row-2nd d-flex justify-content-between">
              <div className="user-tittle d-block">
                <h2 className="font-weight-bold h4 mt-0">
                  {searchuser.firstname} {searchuser.lastname}
                </h2>
              </div>
              {matchwhoami === whoami.username ? (
                <>
                  <div className="button-post d-block">
                    <Button
                      onClick={handleShow}
                      className="post-btn btn btn-purple f-12 font-weight-bold px-2 py-1 f-10"
                    >
                      New Post
                    </Button>
                  </div>
                  <div className="button-post d-block">
                    <button
                      // href="/accounts/journals_editor"
                      onClick={onClick}
                      className="create-journal-btn btn btn-purple f-12 font-weight-bold px-2 py-1 f-10"
                    >
                      Create Journals
                    </button>
                  </div>
                </>
              ) : null}
            </div>
          </div>
          <div className="row second-col-2nd-row">
            <div className="mt-3">
              <NewlineText bio={searchuser.bio} />
            </div>
          </div>
          <div className="row second-col-2nd-row mt-5">
            <div className="text-center f-10">
              <span>{postsuserid === undefined ? 0 : postsuserid.length}</span>
              <span> Posts</span>
            </div>
            <div className="text-center f-10 ml-5">
              <span>
                {publicjournalsuser === undefined
                  ? 0
                  : publicjournalsuser.length}
              </span>
              <span> Journals</span>
            </div>
          </div>
        </div>
      </Row>
      <div className="row stor-page-second-row">
        <div className="col-lg-8 m-auto p-0 col-feed-profile-page">
          <ul className="nav nav-tabs nav-fill mb-5">
            <li className="nav-item">
              <Link
                className={
                  posts && match.pathname === `/${searchuser.username}`
                    ? "nav-link active text-bold"
                    : "nav-link text-muted"
                }
                onClick={() => setPosts(true) || setJournals(false)}
                to={`/${searchuser.username}`}
              >
                <span className="pr-2">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M21 18H3"
                      stroke={
                        posts && match.pathname === `/${searchuser.username}`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M21 14H3"
                      stroke={
                        posts && match.pathname === `/${searchuser.username}`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M21 10H3"
                      stroke={
                        posts && match.pathname === `/${searchuser.username}`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M21 6H3"
                      stroke={
                        posts && match.pathname === `/${searchuser.username}`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                </span>
                <span
                  className={
                    `font-weight-bold text-uppercase f-10` +
                    (posts && match.pathname === `/${searchuser.username}`
                      ? ` text-black p-absolute-storage-nav-clicked`
                      : ` text-muted p-absolute-storage-nav`)
                  }
                  style={{ position: "absolute" }}
                >
                  Posts
                </span>
              </Link>
            </li>
            <li className="nav-item">
              <Link
                className={
                  journals ||
                  match.pathname === `/${searchuser.username}/journals`
                    ? "nav-link active text-bold"
                    : "nav-link text-muted"
                }
                onClick={() => setJournals(true) || setPosts(false)}
                to={`/${searchuser.username}/journals`}
              >
                <span className="pr-2">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M4 19.5C4 18.837 4.26339 18.2011 4.73223 17.7322C5.20107 17.2634 5.83696 17 6.5 17H20"
                      stroke={
                        journals ||
                        match.pathname === `/${searchuser.username}/journals`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                    <path
                      d="M6.5 2H20V22H6.5C5.83696 22 5.20107 21.7366 4.73223 21.2678C4.26339 20.7989 4 20.163 4 19.5V4.5C4 3.83696 4.26339 3.20107 4.73223 2.73223C5.20107 2.26339 5.83696 2 6.5 2V2Z"
                      stroke={
                        journals ||
                        match.pathname === `/${searchuser.username}/journals`
                          ? "black"
                          : "#8e8e8e"
                      }
                      stroke-width="2"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                    />
                  </svg>
                </span>
                <span
                  className={
                    `font-weight-bold text-uppercase f-10` +
                    (journals ||
                    match.pathname === `/${searchuser.username}/journals`
                      ? ` text-black p-absolute-storage-nav-clicked`
                      : ` text-muted p-absolute-storage-nav`)
                  }
                  style={{ position: "absolute" }}
                >
                  Journals
                </span>
              </Link>
            </li>
          </ul>
          <Switch>
            <Route exact path={`/${searchuser.username}`}>
              <div className="row justify-content-center">
                <div className="col-lg-public-user-page col-md-6 col-sm-12">
                  <UserPublicPost postsuserid={postsuserid} />
                </div>
              </div>
            </Route>
          </Switch>
          <Switch>
            <Route path={`/${searchuser.username}/journals`}>
              <JournalsPublic
                whoamiuserid={whoami.user_id}
                publicjournalsuser={publicjournalsuser}
                loading={loadingPublicJournals}
              />
            </Route>
          </Switch>
        </div>
      </div>
      <div className="row justify-content-center"></div>
    </>
  );
};

export default PublicUserDetail;
