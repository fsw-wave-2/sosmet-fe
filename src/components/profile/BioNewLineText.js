import React from "react";

export default function NewlineText({ bio }) {
  if (bio === null || bio === undefined) {
    return <p></p>;
  } else {
    return bio.split("\n").map((str) => <p className="m-0 p-0 f-12">{str}</p>);
  }
}
