import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import userNoPict from "../../assets/images/user_no-pict.jpg";

import UsersService from "../../services/usersService";
import ChangeAvaFormModal from "../modals/changeAvaModal";
import ContentLoader from "react-content-loader";

import DatePicker from "react-datepicker";

import "react-datepicker/dist/react-datepicker.css";

class FormEditProfile extends Component() {
  constructor(props) {
    super();
    this.state = {
      loading: true,
      userProfile: null,
      show: false,
      // startDate: new Date(),
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    // UsersService.whoami(localStorage.jwtToken)
    //   .then((res) => res.data)
    //   .then((res) => {
    //     // let splitBirthday = res.birthdate.split("-");
    //     // let month = Number(splitBirthday[1] - 1);
    //     // let birthdate = new Date(splitBirthday[2], month, splitBirthday[0]);
    //     // this.setState({ userProfile: res, startDate: birthdate });
    //   })
    //   .catch((err) => console.log(err));
  }

  onChange = (e) => {
    let user = this.props.whoami;
    if (e.target.name === "username") {
      user.username = e.target.value;
    } else if (e.target.name === "email") {
      user.email = e.target.value;
    } else if (e.target.name === "firstname") {
      user.firstname = e.target.value;
    } else if (e.target.name === "lastname") {
      user.lastname = e.target.value;
    } else if (e.target.name === "bio") {
      user.bio = e.target.value;
    } else if (e.target.name === "gender") {
      user.gender = e.target.value;
    } else if (e.target.name === "bio") {
      user.bio = e.target.value;
    } else if (e.target.name === "address") {
      user.address = e.target.value;
    }

    this.setState({ userProfile: user });
  };

  handleChange(date) {
    this.setState({ startDate: date });
  }

  handleSubmit = () => {
    let data = {
      address: this.state.userProfile.address,
      avatar: this.state.userProfile.avatar,
      bio: this.state.userProfile.bio,
      // birthdate: this.state.startDate,
      birthdate: this.state.userProfile,
      cloudinary_id: this.state.userProfile.cloudinary_id,
      email: this.state.userProfile.email,
      firstname: this.state.userProfile.firstname,
      gender: this.state.userProfile.gender,
      lastname: this.state.userProfile.lastname,
      username: this.state.userProfile.username,
    };

    UsersService.updateWhoAmi(
      this.props.whoami.user_id,
      data,
      localStorage.jwtToken
    )
      .then((result) => {
        // console.log(result);
      })
      .catch((err) => {
        console.log(err.message);
      });
  };

  //
  //Modal React Bootstrap
  handleShow = () => {
    this.setState({ show: true });
  };

  handleClose = () => {
    this.setState({ show: false });
  };

  render() {
    const { whoami, auth } = this.props;
    const userAva = !whoami.avatar ? userNoPict : whoami.avatar;

    console.log("whoami", whoami);

    if (this.state.userProfile === null) {
      return null;
    }

    return (
      <>
        <div className="d-flex">
          <div className="ava-wrapper-edit">
            <img
              className="avatar-large rounded-circle"
              src={userAva}
              alt={"Profile Image " + whoami.firstname + whoami.lastname}
              width={150}
              height={150}
            />
          </div>
          <div className="form-image-change">
            <div className="username-label-for-change-image">
              {/* <img src={this.state.file.name} /> */}
              <h2 className="h4 font-weight-bold">{whoami.username}</h2>
            </div>
            <div onClick={this.handleShow} className="change-ava-btn mt-3">
              Change Avatar
            </div>
            <ChangeAvaFormModal
              show={this.state.show}
              handleClose={this.handleClose}
            />
          </div>
        </div>
        <div className="mt-5 mb-5">
          <div className="form-edit-wrapper">
            <div className="form-edit-overflow">
              <Form>
                <Form.Group>
                  <Form.Label className="font-weight-bold">UserName</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.username}
                    onChange={(e) => this.onChange(e)}
                    name="username"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">Email</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.email}
                    onChange={(e) => this.onChange(e)}
                    name="email"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">
                    FirstName
                  </Form.Label>
                  <Form.Control
                    value={this.state.userProfile.firstname}
                    onChange={(e) => this.onChange(e)}
                    name="firstname"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">LastName</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.lastname}
                    onChange={(e) => this.onChange(e)}
                    name="lastname"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">
                    BirthDate
                  </Form.Label>
                  {/* <DatePicker
                    className="textarea form-control"
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    dateFormat="dd MMMM yyyy"
                    name="startDate"
                  /> */}
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">Bio</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.bio}
                    as="textarea"
                    col={3}
                    onChange={(e) => this.onChange(e)}
                    name="bio"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">Address</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.address}
                    as="textarea"
                    col={3}
                    onChange={(e) => this.onChange(e)}
                    name="address"
                  />
                </Form.Group>
                <Form.Group>
                  <Form.Label className="font-weight-bold">Sex</Form.Label>
                  <Form.Control
                    value={this.state.userProfile.gender}
                    as="select"
                    onChange={(e) => this.onChange(e)}
                    name="gender"
                  >
                    <option value="">Choosee</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                  </Form.Control>
                </Form.Group>
              </Form>
            </div>
          </div>
          <Button
            onClick={() => this.handleSubmit()}
            variant="disabled"
            className="btn-purple"
          >
            Save
          </Button>
        </div>
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  whoami: state.whoami.whoami,
  auth: state.auth,
});

// export default FormEditProfile;
export default connect(mapStateToProps)(FormEditProfile);
