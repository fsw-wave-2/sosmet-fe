import React from "react";
import ContentLoader from "react-content-loader";
import JournalsNoThumbnail from "../assets/images/journal_no-thumbnail.png";
import ReactHtmlParser from "react-html-parser";
import { Link } from "react-router-dom";

export const JournalsPublic = ({
  publicjournalsuser,
  props,
  loading,
  whoamiuserid,
}) => {
  //
  //debug
  // console.log("publicjournalsuser:", publicjournalsuser);
  // console.log("loading:", loading);

  if (
    loading === true ||
    // publicjournalsuser.length === 0 ||
    publicjournalsuser === undefined ||
    publicjournalsuser === ""
  ) {
    return (
      <div className="post-detail-link mmm">
        <ContentLoader
          speed={2}
          width={900}
          height={260}
          viewBox="0 0 900 260"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="10" y="12" rx="3" ry="3" width="130" height="200" />
          <rect x="160" y="12" rx="3" ry="3" width="130" height="200" />
          <rect x="310" y="12" rx="3" ry="3" width="130" height="200" />
          <rect x="460" y="12" rx="3" ry="3" width="130" height="200" />
          <rect x="610" y="12" rx="3" ry="3" width="130" height="200" />
        </ContentLoader>
      </div>
    );
  }

  const Journal = (journalData) => {
    const imageThumbnailSrc = !journalData.thumbnail
      ? JournalsNoThumbnail
      : journalData.thumbnail;
    console.log(JournalsNoThumbnail);

    return (
      <a
        href={`/journal/${journalData.journal_id}`}
        key={journalData.journal_id}
        className="journal-detail-link"
      >
        <div className="col card-hover p-2">
          <div className="card card-journal m-0">
            <div
              className="background-image-thumbnail"
              style={{
                backgroundImage: `url(${imageThumbnailSrc})`,
                display: "block",
                width: "100%",
                paddingTop: "133.33%",
              }}
              key={journalData.journal_id}
            ></div>
            <div className="background-shawdow-gradient-text"></div>
            {whoamiuserid === journalData.user.user_id ? (
              <a
                href={`/accounts/journals_editor/${journalData.journal_id}`}
                className="f-10 position-absolute edit-journal-button"
                key={journalData.journal_id}
              >
                edit
              </a>
            ) : null}
            <div className="title-journal">
              {ReactHtmlParser(journalData.title)}
            </div>
          </div>
        </div>
      </a>
    );
  };

  const Journals = publicjournalsuser.map((journalData) =>
    Journal(journalData)
  );

  return (
    <div className="image-item row row-cols-1 row-cols-md-5 row-cols-sm-2 g-6 mt-5 p-4">
      {Journals}
    </div>
  );
};
