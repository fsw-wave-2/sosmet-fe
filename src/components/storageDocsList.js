import React from "react";

export const DocsList = ({ fetchdocs }) => {
  if (fetchdocs.length === 0) return null;

  const DocRow = (docData) => {
    return (
      <div>
        <tr
          key={docData.user_id}
          className={docData.user_id % 2 === 0 ? "odd" : "even"}
        >
          <td>{docData.storage_file_id}</td>
          <td>
            <a href={docData.file_link} download>
              {docData.file_link}
            </a>
          </td>
        </tr>
      </div>
    );
  };

  const DocTable = fetchdocs.map((docData) => DocRow(docData));

  return (
    <div className="doc-items col-5 mt-4">
      <h4>Users Game </h4>
      <table className="table table-bordered">
        <thead>
          <tr>
            <th>Id</th>
            <th>Link Doc</th>
          </tr>
        </thead>
        <tbody>{DocTable}</tbody>
      </table>
    </div>
  );
};
