import React from "react";

export const ImagesList = ({ fetchimages }) => {
  if (fetchimages.length === 0) return null;

  const Image = (imageData) => {
    return (
      <div className="col card-hover p-0">
        <div className="card card-image m-0">
          <img
            src={imageData.image_link}
            height="360px"
            style={{ objectFit: "cover" }}
            alt={`${imageData.user_id}`}
          />
        </div>
      </div>
    );
  };

  const Images = fetchimages.map((imageData) => Image(imageData));

  return (
    <div className="image-item row row-cols-1 row-cols-md-3 row-cols-sm-3 g-6 mt-5 p-4">
      {Images}
    </div>
  );
};
