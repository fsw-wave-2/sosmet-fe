import React from "react";

export const AudiosList = ({ fetchdocs }) => {
  if (fetchdocs.length === 0) return null;

  const DocRow = (docData) => {
    return (
      <div>
        <div key={docData.user_id} className="col col-lg-10 m-auto pb-3">
          <audio
            controls
            className="w-100"
            style={{ borderRadius: "0px", backgroundColor: "#f1f3f4" }}
          >
            <source src={docData.audio_link} type="audio/mpeg" />
          </audio>
        </div>
      </div>
    );
  };

  const DocTable = fetchdocs.map((docData) => DocRow(docData));

  return <div className="doc-items mt-4">{DocTable}</div>;
};
