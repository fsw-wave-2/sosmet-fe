import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { registerUser } from "../../redux/actions/authAction";
import { registerModalHide } from "../../redux/actions/registerModalAction";
import classnames from "classnames";
import { Modal } from "react-bootstrap";

class RegisterModal extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      email: "",
      password: "",
      password2: "",
      errors: {},
      errorPassword: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
    if (this.state.password !== this.state.password2) {
      this.setState({ errorPassword: " unmatched" });
    }
  };

  onSubmit = (e) => {
    e.preventDefault();

    const newUser = {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    };

    // console.log(newUser);
    this.props.registerUser(newUser, this.props.history);
  };

  onHide = (e) => {
    this.props.registerModalHide(e);
  };

  componentDidMount() {
    // If logged in and user navigates to Register page, should redirect to dashboard
    if (this.props.auth.isAuthenticated) {
      // history.push("/home");
    }
  }

  render() {
    const {
      errors,
      username,
      email,
      password,
      password2,
      errorPassword,
    } = this.state;
    // console.log("states", this.state);

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <div className="login-page">
          <div className="container">
            <RegisterForm
              errors={errors}
              onChange={this.onChange}
              onSubmit={this.onSubmit}
              email={email}
              username={username}
              password={password}
              password2={password2}
              errorPassword={errorPassword}
            />
          </div>
        </div>
      </Modal>
    );
  }
}

const RegisterForm = ({
  onSubmit,
  onChange,
  username,
  errors,
  email,
  password,
  password2,
  errorPassword,
}) => {
  return (
    <>
      <div className="row justify-content-center" style={{ marginTop: "16vh" }}>
        <div className="col-8 col-lg-8 col-md-6 col-sm-4">
          <div className="text-center">
            <h4>
              <b>Register</b> below
            </h4>
          </div>
          <div className="row justify-content-center">
            <form
              noValidate
              onSubmit={onSubmit}
              className="justify-content-center col-5 col-lg-7 col-md-4 col-sm-4 mt-5 mb-5"
            >
              <div className="mb-3">
                <label class="form-label">Username</label>
                <input
                  onChange={onChange}
                  value={username}
                  error={errors.username}
                  autoComplete="off"
                  id="username"
                  type="text"
                  className={classnames("auth-form-control form-control", {
                    invalid: errors.username,
                  })}
                />
                <span className="red-text">{errors.username}</span>
              </div>
              <div className="mb-3">
                <label for="email" class="form-label">
                  Email
                </label>
                <input
                  onChange={onChange}
                  value={email}
                  error={errors.email}
                  autoComplete="off"
                  id="email"
                  type="text"
                  className={classnames("auth-form-control form-control", {
                    invalid: errors.email,
                  })}
                />
                <span className="red-text">{errors.email}</span>
              </div>
              <div className="mb-3">
                <label for="password" class="form-label">
                  Password
                </label>
                <input
                  onChange={onChange}
                  value={password}
                  error={errors.password}
                  autoComplete="off"
                  id="password"
                  type="password"
                  className={classnames("auth-form-control form-control", {
                    invalid: errors.password,
                  })}
                />
                <span className="red-text">{errors.password}</span>
              </div>
              <div className="mb-3">
                <label for="password2" class="form-label">
                  Password
                </label>
                <input
                  onChange={onChange}
                  value={password2}
                  error={errors.password}
                  autoComplete="off"
                  id="password2"
                  type="password"
                  className={classnames("auth-form-control form-control", {
                    invalid: errors.password,
                  })}
                />
                {password2 && password !== password2 ? (
                  <p
                    style={{
                      color: "red",
                      fontSize: "11px",
                      paddingTop: "8px",
                      paddingBottom: "8px",
                    }}
                  >
                    {errorPassword}
                  </p>
                ) : (
                  <p></p>
                )}
                <span className="red-text">{errors.password}</span>
              </div>
              <div className="text-center mt-5">
                <span className="text-danger">{errors.message}</span>
              </div>
              <div className="text-center mt-5 f-12">
                <button type="submit" className="btn btn-purple">
                  Sign Up
                </button>
              </div>
            </form>
          </div>
          <div className="text-center my-3">
            <span>
              Have an account? Let's <a href="/accounts/login">Login!</a>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

RegisterModal.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  registerModalHide: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth, //recieve state from authreducer
  errors: state.errors, // recieve state from errorreducer
});

export default connect(mapStateToProps, { registerUser, registerModalHide })(
  withRouter(RegisterModal)
);
