import React, { useState } from "react";
import { Modal } from "react-bootstrap";
import UsersService from "../../services/usersService";
import Plyr from "react-plyr";
import "plyr-react/dist/plyr.css";

const AddImageModal = (props) => {
  //
  //debug
  console.log("AddImageModal props:", props);
  console.log("AddImageModal props.user_id:", props.user_id);

  const [selectedFile, setSelectedFile] = useState(undefined);
  const [currentFile, setCurrentFile] = useState(undefined);
  const [preview, setPreview] = useState();

  console.log(currentFile);

  const selectFile = (e) => {
    setSelectedFile(e.target.files);
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setPreview(objectUrl);

    //
    //debug
    console.log("e.target.files", e.target.files);
    console.log(objectUrl);
  };

  function uploadImage() {
    const user_id = props.user_id;
    const token = localStorage.jwtToken;
    let currentFile = selectedFile[0];

    //
    //debug
    // console.log("user_id:", user_id);
    // console.log("token:", token);
    console.log("currentFile:", currentFile);
    setCurrentFile(currentFile);

    UsersService.uploadImages(user_id, currentFile, token)
      .then((res) => {
        //
        //
        console.log("res:", res);
      })
      .catch((err) => {
        //
        //debug
        console.log("err", err);
      });
  }

  // var fileLocal = selectedFile;

  console.log("url", selectedFile);
  return (
    <Modal
      {...props}
      size="lg-3"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h6 className="h5 mb-3 mt-3 pl-2">
          ✅ Check! Your 🏞 Image File Name 👇:
        </h6>
        <div className="box-file-see row justify-content-center m-2 p-4">
          <div className="h6 f-12">
            <div className="image-container col-lg-3">
              <img src={preview} width="250px" />
            </div>
            <div className="mt-2 text-center font-weight-bold">
              {!selectedFile ? "" : selectedFile[0].name}
            </div>
          </div>
        </div>
        <input type="file" id="avatar-input" onChange={selectFile} hidden />
        <div className="browse-and-upload-btn-group mt-3">
          <label className="ava-form-label btn mr-5" for="avatar-input">
            Browse
          </label>
          <button
            type="submit"
            className="upload-ava-btn btn btn-purple"
            disable={!selectedFile}
            onClick={uploadImage}
          >
            Upload
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const AddVideoModal = (props) => {
  console.log("AddVideoModals props:", props);
  const [selectedFile, setSelectedFile] = useState(undefined);
  const [currentFile, setCurrentFile] = useState(undefined);
  const [preview, setPreview] = useState();

  console.log(currentFile);

  const selectFile = (e) => {
    setSelectedFile(e.target.files);
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setPreview(objectUrl);

    //
    //debug
    // console.log("e.target.files", e.target.files);
  };

  function uploadVideo() {
    const user_id = props.user_id;
    const token = localStorage.jwtToken;
    let currentFile = selectedFile[0];
    //
    //debug
    // console.log("user_id:", user_id);
    // console.log("token:", token);
    // console.log("currentFile:", currentFile);

    setCurrentFile(currentFile);

    UsersService.uploadVideos(user_id, currentFile, token)
      .then((res) => {
        //
        //
        console.log("res:", res);
      })
      .catch((err) => {
        //
        //debug
        console.log("err", err);
      });
  }

  return (
    <Modal
      {...props}
      size="lg-5"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body closeButton>
        <h6 className="h5 mb-3 mt-3 pl-2">
          ✅ Check! Your 🎞 Video File Name 👇:
        </h6>
        <div className="box-file-see text-align-center m-2 p-4">
          <span className="h6 f-12">
            <div className="image-container">
              {!preview ? (
                ""
              ) : (
                <Plyr type="video" url={preview} width="250px" />
              )}
            </div>
            <div className="mt-2 text-center font-weight-bold">
              {!selectedFile ? "" : selectedFile[0].name}
            </div>
          </span>
        </div>
        <input type="file" id="avatar-input" onChange={selectFile} hidden />
        <div className="browse-and-upload-btn-group mt-3">
          <label className="ava-form-label btn mr-5" for="avatar-input">
            Browse
          </label>
          <button
            type="submit"
            className="upload-ava-btn btn btn-purple"
            disable={!selectedFile}
            onClick={uploadVideo}
          >
            Upload
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const AddDocModal = (props) => {
  const [selectedFile, setSelectedFile] = useState(undefined);
  const [currentFile, setCurrentFile] = useState(undefined);

  console.log(currentFile);

  const selectFile = (e) => {
    setSelectedFile(e.target.files);

    //
    //debug
    // console.log("e.target.files", e.target.files);
  };

  function uploadDoc() {
    const user_id = props.user_id;
    const token = localStorage.jwtToken;
    let currentFile = selectedFile[0];
    //
    //debug
    // console.log("user_id:", user_id);
    // console.log("token:", token);
    // console.log("currentFile:", currentFile);

    setCurrentFile(currentFile);

    UsersService.uploadDocs(user_id, currentFile, token)
      .then((res) => {
        //
        //
        console.log("res:", res);
      })
      .catch((err) => {
        //
        //debug
        console.log("err", err);
      });
  }
  return (
    <Modal
      {...props}
      size="lg"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h6 className="h5 mb-3 mt-3 pl-2">
          ✅ Check! Your 📄 Doc File Name 👇:
        </h6>
        <div className="box-file-see text-align-center m-2 p-4">
          <span className="h6 f-12">
            {!selectedFile ? "" : selectedFile[0].name}
          </span>
        </div>
        <input type="file" id="avatar-input" onChange={selectFile} hidden />
        <div className="browse-and-upload-btn-group mt-3">
          <label className="ava-form-label btn mr-5" for="avatar-input">
            Browse
          </label>
          <button
            type="submit"
            className="upload-ava-btn btn btn-purple"
            disable={!selectedFile}
            onClick={uploadDoc}
          >
            Upload
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

const AddAudioModal = (props) => {
  const [selectedFile, setSelectedFile] = useState(undefined);
  const [currentFile, setCurrentFile] = useState(undefined);
  const [preview, setPreview] = useState();

  console.log(currentFile);

  const selectFile = (e) => {
    setSelectedFile(e.target.files);
    const objectUrl = URL.createObjectURL(e.target.files[0]);
    setPreview(objectUrl);

    //
    //debug
    // console.log("e.target.files", e.target.files);
  };

  function uploadAudio() {
    const user_id = props.user_id;
    const token = localStorage.jwtToken;
    let currentFile = selectedFile[0];
    //
    //debug
    // console.log("user_id:", user_id);
    // console.log("token:", token);
    // console.log("currentFile:", currentFile);

    setCurrentFile(currentFile);

    UsersService.uploadAudios(user_id, currentFile, token)
      .then((res) => {
        //
        //
        console.log("res:", res);
      })
      .catch((err) => {
        //
        //debug
        console.log("err", err);
      });
  }
  return (
    <Modal
      {...props}
      size="lg-5"
      aria-labelledby="contained-modal-title-vcenter"
      centered
    >
      <Modal.Body>
        <h6 className="h5 mb-3 mt-3 pl-2">
          ✅ Check! Your 🎙 Audio File Name 👇:
        </h6>
        <div className="box-file-see text-align-center m-2 p-4">
          <span className="h6 f-12">
            <div className="audio-container">
              {!preview ? (
                ""
              ) : (
                <audio
                  controls
                  className="w-100"
                  style={{ borderRadius: "0px", backgroundColor: "#f1f3f4" }}
                >
                  <source src={preview} type="audio/mpeg" />
                </audio>
              )}
            </div>
            <div className="mt-2 text-center font-weight-bold">
              {!selectedFile ? "" : selectedFile[0].name}
            </div>
          </span>
        </div>
        <input type="file" id="avatar-input" onChange={selectFile} hidden />
        <div className="browse-and-upload-btn-group mt-3">
          <label className="ava-form-label btn mr-5" for="avatar-input">
            Browse
          </label>
          <button
            type="submit"
            className="upload-ava-btn btn btn-purple"
            disable={!selectedFile}
            onClick={uploadAudio}
          >
            Upload
          </button>
        </div>
      </Modal.Body>
    </Modal>
  );
};

export { AddImageModal, AddVideoModal, AddDocModal, AddAudioModal };
