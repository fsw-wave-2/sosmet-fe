import React, { Component } from "react";
import { Link, useParams } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loginUser } from "../../redux/actions/authAction";
import { Modal } from "react-bootstrap";
import history from "../../history";
import { loginModalHide } from "../../redux/actions/loginModalAction";

class LoginModal extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {},
    };
  }

  componentWillReceiveProps(nextProps) {
    //
    //debug
    // console.log(
    //   "this.props.auth.isAuthenticated:",
    //   this.props.auth.isAuthenticated
    // );
    // console.log("componentWilReceiveProps");
    if (nextProps.auth.isAuthenticated) {
      this.props.loginModalHide();
    }
    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors,
      });
    }
  }

  onChange = (e) => {
    this.setState({ [e.target.id]: e.target.value });
  };

  onSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: this.state.email,
      password: this.state.password,
    };
    // console.log(userData);
    this.props.loginUser(userData);
  };

  onHide = (e) => {
    this.props.loginModalHide(e);
  };

  // componentDidMount() {
  //   console.log(
  //     "this.props.auth.isAuthenticated:",
  //     this.props.auth.isAuthenticated
  //   );
  //   // If logged in and user navigates to Login page, should redirect to dashboard
  //   if (this.props.auth.isAuthenticated) {
  //     this.props.history.push("/home");
  //   }
  // }

  render() {
    const { errors, email, password } = this.state;
    //
    //debug
    // console.log("this.props", this.props);

    return (
      <Modal
        {...this.props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <div className="login-page">
          <div className="container">
            <LoginForm
              errors={errors}
              onChange={this.onChange}
              onSubmit={this.onSubmit}
              email={email}
              password={password}
            />
          </div>
        </div>
      </Modal>
    );
  }
}

const LoginForm = ({ errors, onSubmit, email, onChange, password }) => {
  return (
    <>
      <div className="row justify-content-center" style={{ marginTop: "16vh" }}>
        <div className="col-8 col-lg-8 col-md-4 col-sm-4">
          <div className="col-12">
            <h4 className="text-center">
              <b>Login</b> below
            </h4>
          </div>
          <div className="row justify-content-center">
            <form
              noValidate
              onSubmit={onSubmit}
              className="justify-content-center col-5 col-lg-7 col-md-4 col-sm-4 mt-5 mb-5"
            >
              <div className="mb-3">
                <label class="form-label">Email</label>
                <input
                  onChange={onChange}
                  value={email}
                  error={errors.email}
                  autoComplete="off"
                  id="email"
                  type="text"
                  className="auth-form-control form-control"
                />
                <span className="red-text">{errors.email} </span>
              </div>
              <div className="mb-3">
                <label class="form-label">Password</label>
                <input
                  onChange={onChange}
                  value={password}
                  error={errors.password}
                  autoComplete="off"
                  id="password"
                  type="password"
                  className="auth-form-control form-control"
                />
                <span className="red-text">{errors.password} </span>
              </div>
              <div className="text-center mt-5">
                <span className="text-danger">{errors.message}</span>
              </div>
              <div className="text-center mt-5 f-12">
                <button type="submit" className="btn btn-purple">
                  Login
                </button>
              </div>
            </form>
          </div>

          <div className="text-center my-3">
            <span>
              Don't have an account? Let's{" "}
              <a href="/accounts/register" onClick={(e) => this.onHide()}>
                Create Account
              </a>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

LoginModal.propTypes = {
  loginUser: PropTypes.func.isRequired,
  loginModalHide: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors,
});

export default connect(mapStateToProps, { loginUser, loginModalHide })(
  LoginModal
);
