import React from "react";
import moment from "moment";
import ContentLoader from "react-content-loader";
import Plyr from "react-plyr";
// import Plyr from "plyr-react";
import "plyr-react/dist/plyr.css";

export const VideoDetailsDescription = ({
  loading,
  props,
  storagevideodetails,
}) => {
  if (loading === true || storagevideodetails.length === 0) {
    return (
      <div className="post-detail-link">
        <ContentLoader
          speed={2}
          width={1040}
          height={860}
          viewBox="0 0 1040 860"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="0" y="40" rx="3" ry="3" width="1040" height="560" />
          <rect x="0" y="620" rx="3" ry="3" width="120" height="20" />
        </ContentLoader>
      </div>
    );
  }

  const date = moment(storagevideodetails.createdAt).format("DD MMMM YYYY");
  const time = moment(storagevideodetails.createdAt).fromNow();

  console.log("firstname", storagevideodetails.user);

  let Fullname =
    (storagevideodetails.user.firstname === undefined &&
      storagevideodetails.user.lastname === undefined) ||
    (storagevideodetails.user.firstname === null &&
      storagevideodetails.user.lastname === null)
      ? storagevideodetails.user.username + storagevideodetails.user_id
      : `${storagevideodetails.user.firstname} ${storagevideodetails.user.lastname}`;

  return (
    <>
      <div className="mt-5">
        <Plyr type="video" url={storagevideodetails.video_link} />
        {/* <Plyr sources={storagevideodetails.video_link} /> */}
        <div className="date-and-time pl-1 pt-3 f-12 font-weight-bold text-capitalize">
          <div className="vide-page-detail-container">
            <div className="author-detail-journal-time-stamps">
              <div className="avatar-author ">
                <img
                  src={storagevideodetails.user.avatar}
                  alt={`Avatar Of ${Fullname}`}
                  className="avatar-journal"
                />
              </div>
              <div className="author-name-time-stamp-reading-time">
                <div className="author-name">
                  <span>
                    <a href={`/${storagevideodetails.user.username}`}>
                      {Fullname}
                    </a>
                  </span>
                </div>
                <div className="time-stamp-reading-time">
                  <div className="time-stamp-journal">
                    <span>{date}</span>
                  </div>
                  <div className="reading-time">
                    <span>Upload </span>
                    <span>{time}</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="share-social-button">
              <ul className="social-icon-link">
                <li className="share-link-list">
                  <a
                    href={storagevideodetails.video_link}
                    className="download-icon-video-detail"
                    download
                  >
                    <svg
                      width="24"
                      height="24"
                      viewBox="0 0 24 24"
                      fill="none"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        d="M21 15V19C21 19.5304 20.7893 20.0391 20.4142 20.4142C20.0391 20.7893 19.5304 21 19 21H5C4.46957 21 3.96086 20.7893 3.58579 20.4142C3.21071 20.0391 3 19.5304 3 19V15"
                        stroke="purple"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M7 10L12 15L17 10"
                        stroke="purple"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                      <path
                        d="M12 15V3"
                        stroke="purple"
                        stroke-width="2"
                        stroke-linecap="round"
                        stroke-linejoin="round"
                      />
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
