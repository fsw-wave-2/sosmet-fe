import React from "react";

export const VideosList = ({ fetchvideos }) => {
  if (fetchvideos.length === 0) return null;

  const Video = (videoData) => {
    return (
      <div className="col card-hover p-0">
        <div className="card card-video">
          <a href={`/accounts/video/${videoData.storage_video_id}`}>
            <video src={videoData.video_link} height="240px" />
          </a>
        </div>
      </div>
    );
  };

  const Videos = fetchvideos.map((videoData) => Video(videoData));

  return (
    <div className="video-item row row-cols-1 row-cols-md-3 g-4">{Videos}</div>
  );
};
