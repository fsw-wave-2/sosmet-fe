import React from "react";
import { Link } from "react-router-dom";
// import { useDispatch } from "react-redux";
import AddPostHomePage from "./AddPostHomePage";
import userNoPict from "../../assets/images/user_no-pict.jpg";
import ReactHtmlParser from "react-html-parser";
import ContentLoader from "react-content-loader";
import moment from "moment";

export const Feeds = ({
  publicposts,
  auth,
  props,
  loves,
  commentspost,
  // postdetails,
}) => {
  // if (publicusers.length === 0) return null;

  //
  //debug
  // console.log(publicusers.length);f
  // console.log("FEEEEEDS:", whoami.length === 0);

  // const dispatch = useDispatch();

  // dispatch(fetchPostLovesStart());

  if (publicposts === undefined || publicposts.length === 0) {
    return (
      <ContentLoader
        speed={2}
        width={900}
        height={460}
        viewBox="0 0 900 460"
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
      >
        <rect x="38" y="7" rx="1" ry="1" width="90" height="6" />
        <rect x="38" y="18" rx="1" ry="1" width="60" height="6" />
        <rect x="0" y="52" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="72" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="92" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="112" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="132" rx="1" ry="1" width="400" height="10" />
        <circle cx="15" cy="15" r="15" />

        <rect x="38" y="182" rx="1" ry="1" width="90" height="6" />
        <rect x="38" y="192" rx="1" ry="1" width="60" height="6" />
        <rect x="0" y="222" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="242" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="262" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="282" rx="1" ry="1" width="600" height="10" />
        <rect x="0" y="302" rx="1" ry="1" width="400" height="10" />
        <circle cx="15" cy="190" r="15" />
      </ContentLoader>
    );
  }

  const Feed = (publicPost) => {
    const userAva = !publicPost.user.avatar
      ? userNoPict
      : publicPost.user.avatar;

    const Fullname =
      (publicPost.user.firstname === "" && publicPost.user.lastname === "") ||
      (publicPost.user.firstname === null && publicPost.user.lastname === null)
        ? publicPost.user.username + publicPost.user_id
        : `${publicPost.user.firstname} ${publicPost.user.lastname}`;

    const date = moment(publicPost.createdAt).format("DD MMMM YYYY");
    // const time = moment(publicPost.createdAt).format("HH:MM");
    const time = moment(publicPost.createdAt).fromNow();

    return (
      // <a className="post-detail-link" href={"/post/" + publicPost.post_id}>
      // <Link
      //   className="post-detail-link"
      //   to={"/feed-" + publicPost.user.username + "-" + publicPost.post_id}
      // >
      // <Link className="post-detail-link" to={"/feed-" + publicPost.post_id}>
      <Link
        className="post-detail-link col-lg col-md-8 col-sm-12"
        to={`/post/${publicPost.post_id}`}
      >
        <div className="quotes-people-box-list">
          <div className="first-text d-flex justify-content-between">
            <div className="img-bio d-flex justify-content-between">
              <div className="image-profile-box">
                <div className="image-profile">
                  <img
                    className="avatar-small rounded-circle"
                    src={userAva}
                    alt={`${
                      publicPost.user.firstname + publicPost.user.lastname
                    } Profil`}
                    width="56"
                    height="56"
                  />
                </div>
              </div>
              <div className="bio-desc pl-2 f-10">
                {/* <Link to={"/" + publicPost.user.username}> */}
                <Link
                  to={`/${publicPost.user.username}`}
                  style={{ textDecoration: "none" }}
                >
                  <div className="username">{publicPost.user.username}</div>
                </Link>

                <div className="name font-weight-light">{Fullname}</div>
              </div>
            </div>
            {/* <div className="follow-button-group">
              <button className="follow-btn btn" to="#">
                Follow
              </button>
            </div> */}
          </div>
          <div className="second-text">
            <div className="col" style={{ marginLeft: "38px" }}>
              <p className="row feeds-post f-12">
                {ReactHtmlParser(publicPost.content)}
              </p>
              <div className="row">
                {!publicPost.image ? null : (
                  <img
                    src={publicPost.image}
                    className="rounded"
                    height="280"
                    alt={`Post From ${Fullname}`}
                  />
                )}
              </div>
            </div>
          </div>

          <div className="thrid-text col pt-5 pb-5">
            <div className="row justify-content-between">
              <div className="third-text-left-group d-flex">
                <div className="text-muted f-10 font-weight-normal">
                  <span>{date}</span>
                </div>
              </div>
              <div className="third-text-right-group">
                <div className="ml-auto">
                  <p className="mb-0 text-muted f-10 font-weight-normal">
                    {/* <span className="comment-info-numbers">
                      {!commentspost ? 0 : commentspost.length} Comment
                    </span>
                    <span className="love-info-numbers">
                      {!loves ? 0 : loves.count} Loved
                    </span> */}
                    Post From
                    {publicPost.client_device === null ? (
                      <span> 🌏 Earth</span>
                    ) : (
                      <span> {publicPost.client_device}</span>
                    )}
                    <span className="separator-text text-capitalize">
                      {time}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Link>
    );
  };

  const Feedlist = publicposts.map((publicPost) => Feed(publicPost));

  return auth.isAuthenticated === true ? (
    <div className="child-home-page">
      <div className="child-home-page-wrapper">
        <div className="card-no-round">
          <AddPostHomePage />
        </div>
        <div className="public-feeds-home-relative">
          <div className="public-feeds-home">
            <div className="public-feeds-home-wrapper">{Feedlist}</div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className="child-landing-page">
      <div className="child-landing-page-wrapper">
        <div className="public-feeds-landing">{Feedlist}</div>
      </div>
    </div>
  );
};
