import React from "react";
import { Link } from "react-router-dom";
// import { Link } from "react-dom";
import ReactHtmlParser from "react-html-parser";
import userNoPict from "../../assets/images/user_no-pict.jpg";
import ContentLoader from "react-content-loader";
import moment from "moment";

export const UserPublicPost = ({ postsuserid, props }) => {
  console.log(postsuserid);
  if (postsuserid === undefined || postsuserid.length === 0) {
    return (
      <div className="post-detail-link mmm">
        <ContentLoader
          speed={2}
          width={900}
          height={260}
          viewBox="0 0 900 260"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="78" y="12" rx="3" ry="3" width="140" height="12" />
          <rect x="78" y="36" rx="3" ry="3" width="90" height="12" />
          <rect x="0" y="92" rx="3" ry="3" width="600" height="14" />
          <rect x="0" y="116" rx="3" ry="3" width="600" height="14" />
          <rect x="0" y="140" rx="3" ry="3" width="600" height="14" />
          <circle cx="30" cy="30" r="30" />
        </ContentLoader>
      </div>
    );
  }

  const PostRow = (post, index) => {
    const userAva = !post.user.avatar ? userNoPict : post.user.avatar;

    const Fullname =
      (post.user.firstname === "" && post.user.lastname === "") ||
      (post.user.firstname === null && post.user.lastname === null)
        ? post.user.username + post.user_id
        : `${post.user.firstname} ${post.user.lastname}`;

    const date = moment(post.createdAt).format("DD MMMM YYYY");
    // const time = moment(publicPost.createdAt).format("HH:MM");
    const time = moment(post.createdAt).fromNow();

    return (
      <a
        // onClick={(e) => openDetail(post)}
        id={index}
        href={`/post/${post.post_id}`}
        className="post-detail-link mmm"
      >
        <div className="quotes-people-box-list">
          <div className="first-text d-flex justify-content-between">
            <div className="img-bio d-flex justify-content-between">
              <div className="image-profile-box">
                <div className="image-profile">
                  <img
                    className="avatar-small img-src rounded-circle"
                    src={userAva}
                    alt={post.user.username + " Profil Picture"}
                    width="56"
                    height="56"
                  />
                </div>
              </div>
              <div className="bio-desc-user-public-posts-page pl-2 f-10">
                <Link to="#" style={{ textDecoration: "none" }}>
                  <div className="username">{post.user.username}</div>
                </Link>
                <div className="name font-weight-light">{Fullname}</div>
                <div className="second-text">
                  <div className="col" style={{ marginLeft: "38" }}>
                    <p className="row feeds-post-public-page f-12">
                      {ReactHtmlParser(post.content)}
                    </p>
                    <div className="row">
                      {!post.image ? null : (
                        <img
                          src={post.image}
                          className="rounded"
                          height="280"
                          alt={`Post From ${Fullname}`}
                        />
                      )}
                    </div>
                  </div>
                </div>
              </div>
            </div>

            {/* <div className="follow-button-group">
                    <button className="follow-btn btn" to="#">
                      Follow
                    </button>
                  </div> */}
          </div>
          <div className="thrid-text col pt-5 pb-5">
            <div className="row justify-content-between">
              <div className="third-text-left-group d-flex">
                <div className="text-muted f-10 font-weight-normal">
                  <span>{date}</span>
                </div>
              </div>
              <div className="third-text-right-group">
                <div className="ml-auto">
                  <p className="mb-0 text-muted f-10 font-weight-normal">
                    {/* <span className="comment-info-numbers">
                      {!commentspost ? 0 : commentspost.length} Comment
                    </span>
                    <span className="love-info-numbers">
                      {!loves ? 0 : loves.count} Loved
                    </span> */}
                    Post From
                    {post.client_device === null ? (
                      <span> 🌏 Earth</span>
                    ) : (
                      <span> {post.client_device}</span>
                    )}
                    <span className="separator-text text-capitalize">
                      {time}
                    </span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </a>
    );
  };

  const PostTable = postsuserid.map((post, index) => PostRow(post, index));

  return <>{PostTable}</>;
};
