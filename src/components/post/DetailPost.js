import React, { Component } from "react";
import { BrowserRouter as Router, useParams } from "react-router-dom";
import { Navbar, Container } from "react-bootstrap";

export class PublicPostDetail extends Component {
  componentDidMount() {
    console.log("this.props:", this.props.match);
  }

  render() {
    return (
      <div className="public-user-page">
        <Navbar />
        <Router>
          <Container className="section-main storage-page p-0">
            <PublicPostDetailDescription />
          </Container>
        </Router>
      </div>
    );
  }
}

const PublicPostDetailDescription = (se) => {
  let { id } = useParams();

  console.log(id);

  return (
    <div>
      <h2>{id}</h2>
    </div>
  );
};
