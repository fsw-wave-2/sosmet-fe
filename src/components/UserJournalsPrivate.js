import React from "react";
import ContentLoader from "react-content-loader";
import JournalsNoThumbnail from "../assets/images/journal_no-thumbnail.png";
import ReactHtmlParser from "react-html-parser";

export const JournalsPrivate = ({
  privatejournalssuser,
  props,
  whoamiuserid,
}) => {
  //
  //debug
  // console.log(privatejournalssuser);
  // console.log("whoamiuserid:", whoamiuserid);

  if (privatejournalssuser === undefined || privatejournalssuser.length === 0) {
    return (
      <div className="post-detail-link mt-5 pt-3">
        <ContentLoader
          speed={2}
          width={1200}
          height={860}
          viewBox="0 0 1200 860"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="0" y="12" rx="3" ry="3" width="200" height="270" />
          <rect x="230" y="12" rx="3" ry="3" width="200" height="270" />
          <rect x="460" y="12" rx="3" ry="3" width="200" height="270" />
          <rect x="690" y="12" rx="3" ry="3" width="200" height="270" />
          <rect x="920" y="12" rx="3" ry="3" width="200" height="270" />

          <rect x="0" y="320" rx="3" ry="3" width="200" height="270" />
          <rect x="230" y="320" rx="3" ry="3" width="200" height="270" />
          <rect x="460" y="320" rx="3" ry="3" width="200" height="270" />
          <rect x="690" y="320" rx="3" ry="3" width="200" height="270" />
          <rect x="920" y="320" rx="3" ry="3" width="200" height="270" />
        </ContentLoader>
      </div>
    );
  }

  const Journal = (journalData) => {
    const imageThumbnailSrc = !journalData.thumbnail
      ? JournalsNoThumbnail
      : journalData.thumbnail;

    //
    //debug
    // console.log(JournalsNoThumbnail);
    // console.log("whoamiuserid:", whoamiuserid);

    return (
      <div className="col card-hover p-2">
        <div className="card card-journal m-0">
          <div
            className="background-image-thumbnail"
            style={{
              backgroundImage: `url(${imageThumbnailSrc})`,
              display: "block",
              width: "100%",
              paddingTop: "133.33%",
            }}
            key={journalData.journal_id}
          ></div>
          <div className="background-shawdow-gradient-text"></div>
          {whoamiuserid === journalData.user.user_id ? (
            <a
              href={`/accounts/journals_editor/${journalData.journal_id}`}
              className="f-10 position-absolute edit-journal-button"
              key={journalData.journal_id}
            >
              edit
            </a>
          ) : null}
          <div className="title-journal">
            {ReactHtmlParser(journalData.title)}
          </div>
        </div>
      </div>
    );
  };

  const Journals = privatejournalssuser.map((journalData) =>
    Journal(journalData)
  );

  return (
    <div className="image-item row row-cols-1 row-cols-md-5 row-cols-sm-2 g-6 mt-5 p-4">
      {Journals}
    </div>
  );
};
