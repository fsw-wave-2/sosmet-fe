import React from "react";
import { CopyToClipboard } from "react-copy-to-clipboard";
import moment from "moment";
import ReactHtmlParser from "react-html-parser";
import ContentLoader from "react-content-loader";
import readingtime from "reading-time";

export const ContentJournalSection = ({
  publicjournaldetail,
  loading,
  props,
}) => {
  if (
    loading === true ||
    publicjournaldetail.length == 0 ||
    publicjournaldetail === undefined ||
    publicjournaldetail === ""
  ) {
    return (
      <div className="post-detail-link p-0">
        <ContentLoader
          speed={2}
          width={1110}
          height={860}
          viewBox="0 0 1110 860"
          backgroundColor="#f3f3f3"
          foregroundColor="#ecebeb"
          {...props}
        >
          <rect x="90" y="0" rx="3" ry="3" width="200" height="34" />
          <rect x="300" y="0" rx="3" ry="3" width="490" height="34" />

          <circle cx="120" cy="90" r="30" />
          <rect x="170" y="70" rx="3" ry="3" width="140" height="12" />
          <rect x="170" y="90" rx="3" ry="3" width="90" height="12" />
          <rect x="90" y="150" rx="3" ry="3" width="600" height="18" />
          <rect x="700" y="150" rx="3" ry="3" width="290" height="18" />
          <rect x="90" y="180" rx="3" ry="3" width="100" height="18" />
          <rect x="200" y="180" rx="3" ry="3" width="790" height="18" />
          <rect x="90" y="210" rx="3" ry="3" width="400" height="18" />
          <rect x="500" y="210" rx="3" ry="3" width="490" height="18" />

          <rect x="90" y="240" rx="3" ry="3" width="600" height="18" />
          <rect x="700" y="240" rx="3" ry="3" width="290" height="18" />
          <rect x="90" y="270" rx="3" ry="3" width="100" height="18" />
          <rect x="200" y="270" rx="3" ry="3" width="790" height="18" />
          <rect x="90" y="300" rx="3" ry="3" width="400" height="18" />
          <rect x="500" y="300" rx="3" ry="3" width="490" height="18" />

          <rect x="90" y="330" rx="3" ry="3" width="600" height="18" />
          <rect x="700" y="330" rx="3" ry="3" width="290" height="18" />
          <rect x="90" y="360" rx="3" ry="3" width="100" height="18" />
          <rect x="200" y="360" rx="3" ry="3" width="790" height="18" />
          <rect x="90" y="390" rx="3" ry="3" width="400" height="18" />
          <rect x="500" y="390" rx="3" ry="3" width="490" height="18" />

          <rect x="90" y="420" rx="3" ry="3" width="600" height="18" />
          <rect x="700" y="420" rx="3" ry="3" width="290" height="18" />
          <rect x="90" y="450" rx="3" ry="3" width="100" height="18" />
          <rect x="200" y="450" rx="3" ry="3" width="790" height="18" />
          <rect x="90" y="480" rx="3" ry="3" width="400" height="18" />
          <rect x="500" y="480" rx="3" ry="3" width="490" height="18" />
        </ContentLoader>
      </div>
    );
  }

  const date = moment(publicjournaldetail.createdAt).format("DD MMMM YYYY");
  let Fullname =
    (publicjournaldetail.user.firstname === undefined &&
      publicjournaldetail.user.lastname === undefined) ||
    (publicjournaldetail.user.firstname === null &&
      publicjournaldetail.user.lastname === null)
      ? publicjournaldetail.user.username + publicjournaldetail.user_id
      : `${publicjournaldetail.user.firstname} ${publicjournaldetail.user.lastname}`;

  const readingTime = readingtime(publicjournaldetail.content);
  //
  //
  // console.log("readingTime:", readingTime);

  return (
    <>
      <article className="journal-article-container py-5">
        <header className="entry-header">
          <h1 className="entry-title">{publicjournaldetail.title}</h1>
        </header>
        <div className="author-detail-share-button-container">
          <div className="author-detail-journal-time-stamps">
            <div className="avatar-author ">
              <img
                src={publicjournaldetail.user.avatar}
                alt={`Avatar Of ${Fullname}`}
                className="avatar-journal"
              />
            </div>
            <div className="author-name-time-stamp-reading-time">
              <div className="author-name">
                <span>
                  <a href={`/${publicjournaldetail.user.username}`}>
                    {Fullname}
                  </a>
                </span>
              </div>
              <div className="time-stamp-reading-time">
                <div className="time-stamp-journal">
                  <span>{date}</span>
                </div>
                <div className="reading-time">
                  <span>{readingTime.text}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="share-social-button">
            <ul className="social-icon-link">
              <li className="share-link-list">
                <a href="#" className="share-link">
                  <svg
                    id="fb-logo"
                    viewBox="0 0 512 512"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      class=""
                      d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </a>
              </li>
              <li className="share-link-list">
                <a href="#" className="share-link">
                  <svg
                    id="twt-logo"
                    viewBox="0 0 512 512"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      class=""
                      d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </a>
              </li>
              <li className="share-link-list">
                <a href="#" className="share-link">
                  <svg
                    id="in-logo"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 448 512"
                  >
                    <path
                      fill="currentColor"
                      d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"
                      class=""
                    ></path>
                  </svg>
                </a>
              </li>
              <li className="share-link-list">
                <a href="#" className="share-link">
                  <svg
                    id="wa-logo"
                    viewBox="0 0 448 512"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      class=""
                      d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"
                      fill="currentColor"
                    ></path>
                  </svg>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div className="entry-content">
          {ReactHtmlParser(publicjournaldetail.content)}
        </div>
      </article>
      <div className="author-bottom-share-claps-download-group">
        <div className="claps-share-download-group">
          <div className="icon-download">
            <div className="svg-coontainer">
              <svg
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  d="M8.29 13.29C8.19627 13.383 8.12188 13.4936 8.07111 13.6154C8.02034 13.7373 7.9942 13.868 7.9942 14C7.9942 14.132 8.02034 14.2627 8.07111 14.3846C8.12188 14.5064 8.19627 14.617 8.29 14.71L11.29 17.71C11.383 17.8037 11.4936 17.8781 11.6154 17.9289C11.7373 17.9797 11.868 18.0058 12 18.0058C12.132 18.0058 12.2627 17.9797 12.3846 17.9289C12.5064 17.8781 12.617 17.8037 12.71 17.71L15.71 14.71C15.8983 14.5217 16.0041 14.2663 16.0041 14C16.0041 13.7337 15.8983 13.4783 15.71 13.29C15.5217 13.1017 15.2663 12.9959 15 12.9959C14.7337 12.9959 14.4783 13.1017 14.29 13.29L13 14.59V3C13 2.73478 12.8946 2.48043 12.7071 2.29289C12.5196 2.10536 12.2652 2 12 2C11.7348 2 11.4804 2.10536 11.2929 2.29289C11.1054 2.48043 11 2.73478 11 3V14.59L9.71 13.29C9.61704 13.1963 9.50644 13.1219 9.38458 13.0711C9.26272 13.0203 9.13201 12.9942 9 12.9942C8.86799 12.9942 8.73728 13.0203 8.61542 13.0711C8.49356 13.1219 8.38296 13.1963 8.29 13.29ZM18 9H16C15.7348 9 15.4804 9.10536 15.2929 9.29289C15.1054 9.48043 15 9.73478 15 10C15 10.2652 15.1054 10.5196 15.2929 10.7071C15.4804 10.8946 15.7348 11 16 11H18C18.2652 11 18.5196 11.1054 18.7071 11.2929C18.8946 11.4804 19 11.7348 19 12V19C19 19.2652 18.8946 19.5196 18.7071 19.7071C18.5196 19.8946 18.2652 20 18 20H6C5.73478 20 5.48043 19.8946 5.29289 19.7071C5.10536 19.5196 5 19.2652 5 19V12C5 11.7348 5.10536 11.4804 5.29289 11.2929C5.48043 11.1054 5.73478 11 6 11H8C8.26522 11 8.51957 10.8946 8.70711 10.7071C8.89464 10.5196 9 10.2652 9 10C9 9.73478 8.89464 9.48043 8.70711 9.29289C8.51957 9.10536 8.26522 9 8 9H6C5.20435 9 4.44129 9.31607 3.87868 9.87868C3.31607 10.4413 3 11.2044 3 12V19C3 19.7956 3.31607 20.5587 3.87868 21.1213C4.44129 21.6839 5.20435 22 6 22H18C18.7956 22 19.5587 21.6839 20.1213 21.1213C20.6839 20.5587 21 19.7956 21 19V12C21 11.2044 20.6839 10.4413 20.1213 9.87868C19.5587 9.31607 18.7956 9 18 9Z"
                  fill="black"
                />
              </svg>
            </div>
            <div className="title-icon-download">Download</div>
          </div>
          <div className="share-button-bottom">
            <span className="title-share">share this journal to:</span>
            <div className="share-social-button-bottom">
              <ul className="social-icon-link p-0">
                <li className="share-link-list">
                  <a href="#" className="share-link">
                    <svg
                      id="fb-logo"
                      viewBox="0 0 512 512"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        class=""
                        d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </a>
                </li>
                <li className="share-link-list">
                  <a href="#" className="share-link">
                    <svg
                      id="twt-logo"
                      viewBox="0 0 512 512"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        class=""
                        d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </a>
                </li>
                <li className="share-link-list">
                  <a href="#" className="share-link">
                    <svg
                      id="in-logo"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 448 512"
                    >
                      <path
                        fill="currentColor"
                        d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"
                        class=""
                      ></path>
                    </svg>
                  </a>
                </li>
                <li className="share-link-list">
                  <a href="#" className="share-link">
                    <svg
                      id="wa-logo"
                      viewBox="0 0 448 512"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <path
                        class=""
                        d="M380.9 97.1C339 55.1 283.2 32 223.9 32c-122.4 0-222 99.6-222 222 0 39.1 10.2 77.3 29.6 111L0 480l117.7-30.9c32.4 17.7 68.9 27 106.1 27h.1c122.3 0 224.1-99.6 224.1-222 0-59.3-25.2-115-67.1-157zm-157 341.6c-33.2 0-65.7-8.9-94-25.7l-6.7-4-69.8 18.3L72 359.2l-4.4-7c-18.5-29.4-28.2-63.3-28.2-98.2 0-101.7 82.8-184.5 184.6-184.5 49.3 0 95.6 19.2 130.4 54.1 34.8 34.9 56.2 81.2 56.1 130.5 0 101.8-84.9 184.6-186.6 184.6zm101.2-138.2c-5.5-2.8-32.8-16.2-37.9-18-5.1-1.9-8.8-2.8-12.5 2.8-3.7 5.6-14.3 18-17.6 21.8-3.2 3.7-6.5 4.2-12 1.4-32.6-16.3-54-29.1-75.5-66-5.7-9.8 5.7-9.1 16.3-30.3 1.8-3.7.9-6.9-.5-9.7-1.4-2.8-12.5-30.1-17.1-41.2-4.5-10.8-9.1-9.3-12.5-9.5-3.2-.2-6.9-.2-10.6-.2-3.7 0-9.7 1.4-14.8 6.9-5.1 5.6-19.4 19-19.4 46.3 0 27.3 19.9 53.7 22.6 57.4 2.8 3.7 39.1 59.7 94.8 83.8 35.2 15.2 49 16.5 66.6 13.9 10.7-1.6 32.8-13.4 37.4-26.4 4.6-13 4.6-24.1 3.2-26.4-1.3-2.5-5-3.9-10.5-6.6z"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div className="author-details-group">
          <div className="author-details-body">
            <div className="avatar-journal-bottom-container">
              <img
                src={publicjournaldetail.user.avatar}
                alt={`Avatar Of ${Fullname}`}
                className="avatar-journal-bottom"
              />
            </div>
            <div className="author-details-content">
              <div className="title-written-row pb-2">
                <div className="title-written-text">written by</div>
              </div>
              <div className="author-name-follow-btn pb-2">
                <div className="heading-text-author-name">
                  <a
                    href={`/${publicjournaldetail.user.username}`}
                    className="link-profile-journal-bottom"
                  >
                    {Fullname}
                  </a>
                </div>
                <div className="follow-btn-on-journal"></div>
              </div>
              <div className="author-bio-row">
                <div className="author-bio-content-on-journal">
                  {ReactHtmlParser(publicjournaldetail.user.bio)}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};
