import React from "react";
import { Modal } from "react-bootstrap";
import userNoPict from "../../assets/images/user_no-pict.jpg";
import ContentLoader from "react-content-loader";

const SearchResult = ({ show, onHide, result, props, loading, error }) => {
  console.log("show:", show, "\n", "onHide:", onHide, "\n", "loading", loading);

  if (loading == true) {
    return (
      // <Modal
      <div
        // onHide={onHide}
        // show={show}
        // backdropClassName="search-modal-backdrop"
        // aria-labelledby="contained-modal-title-vcenter"
        // enforceFocus={false}
        // keyboard={false}
        // centered
        // className="search-modal-body"
        className="d-block position-absolute search-result"
      >
        <div className="dashboard-link ">
          <div className="p-4">
            <ContentLoader
              speed={2}
              width={175}
              height={32}
              viewBox="0 0 177 34"
              backgroundColor="#f3f3f3"
              foregroundColor="#ecebeb"
              {...props}
            >
              <rect x="38" y="7" rx="1" ry="1" width="90" height="6" />
              <rect x="38" y="18" rx="1" ry="1" width="60" height="6" />

              <circle cx="17" cy="17" r="17" />
            </ContentLoader>
          </div>
        </div>
      </div>
      // {/* </Modal> */}
    );
  }

  return (
    // <Modal
    // onHide={onHide}
    // show={show}
    // backdropClassName="search-modal-backdrop"
    // aria-labelledby="contained-modal-title-vcenter"
    // enforceFocus={false}
    // keyboard={false}
    // className="search-modal-body"
    <div className="d-block position-absolute search-result">
      <div
        className="dashboard-link f-12"
        style={{ minWidth: "228px", minHeight: "64px" }}
      >
        <div
          //   href={"/" + whoami.username}
          className="p-4"
        >
          {/* {result.username === undefined ? (
            <ContentLoader
              speed={2}
              width={175}
              height={32}
              viewBox="0 0 177 34"
              backgroundColor="#f3f3f3"
              foregroundColor="#ecebeb"
              {...props}
            >
              <rect x="38" y="7" rx="1" ry="1" width="90" height="6" />
              <rect x="38" y="18" rx="1" ry="1" width="60" height="6" />

              <circle cx="17" cy="17" r="17" />
            </ContentLoader>
          ) : (
            <a href={`/${result.username}`} style={{ textDecoration: "none" }}>
              <div className="img-bio d-flex ">
                <div className="image-profile-box">
                  <div className="image-profile">
                    <img
                      className="avatar-small rounded-circle"
                      src={userAva}
                      alt={`${result.firstname + result.lastname} Profil`}
                    />
                  </div>
                </div>
                <div className="bio-desc pl-2 f-10">
                  <div className="username">{result.username}</div>

                  <div className="name font-weight-light">{Fullname}</div>
                </div>
              </div>
            </a>
          )} */}
          {/* 
          {error !== null && <span>{error}</span>}

          {result.username === undefined && (
            <ContentLoader
              speed={2}
              width={175}
              height={32}
              viewBox="0 0 177 34"
              backgroundColor="#f3f3f3"
              foregroundColor="#ecebeb"
              {...props}
            >
              <rect x="38" y="7" rx="1" ry="1" width="90" height="6" />
              <rect x="38" y="18" rx="1" ry="1" width="60" height="6" />

              <circle cx="17" cy="17" r="17" />
            </ContentLoader>
          )} */}
          {error !== null ? (
            <div className="font-weight-bold text-purple py-2">{error}</div>
          ) : (
            <a href={`/${result.username}`} style={{ textDecoration: "none" }}>
              <div className="img-bio d-flex ">
                <div className="image-profile-box">
                  <div className="image-profile">
                    <img
                      className="avatar-small rounded-circle"
                      src={!result.avatar ? userNoPict : result.avatar}
                      alt={`${result.firstname + result.lastname} Profil`}
                    />
                  </div>
                </div>
                <div className="bio-desc pl-2 f-10">
                  <div className="username">{result.username}</div>

                  <div className="name font-weight-light">
                    {(result.firstname === "" && result.lastname === "") ||
                    (result.firstname === null && result.lastname === null)
                      ? result.username + result.user_id
                      : `${result.firstname} ${result.lastname}`}
                  </div>
                </div>
              </div>
            </a>
          )}
        </div>
      </div>
    </div>
    // </Modal>
  );
};

export { SearchResult };
