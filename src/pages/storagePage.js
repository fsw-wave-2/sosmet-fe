import React, { Component } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Navbar from "../components/navbar/navbar";
import { connect } from "react-redux";
import { fetchVideos } from "../redux/actions/fetchVideosAction";
import { fetchImages } from "../redux/actions/fetchImagessAction";
import { fetchStorageDocs } from "../redux/actions/fetchStorageDocsAction";
import { fetchStorageAudios } from "../redux/actions/fetchStorageAudiosAction";
// import { fetchImages } from "../redux/actions/fetchImagessAction";
import ReactPlayer from "react-player/youtube";
import { StorageAddButton } from "../components/buttons/StorageAddButton";
import PropTypes from "prop-types";

import { VideosList } from "../components/storageVideoList";
import { ImagesList } from "../components/StorageImageList";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import {
  AddImageModal,
  AddVideoModal,
  AddDocModal,
  AddAudioModal,
} from "../components/modals/UploadStorageModals";
import NewlineText from "../components/profile/BioNewLineText";
import userNoPict from "../assets/images/user_no-pict.jpg";
import ContentLoader from "react-content-loader";
import { DocsList } from "../components/storageDocsList";
import { AudiosList } from "../components/storageAudiosList";

export const x = async ({ fetchvideos }) => {
  const x = await fetchvideos;
  //
  //debug
  // console.log("fetchvideos:\n", fetchvideos);
  // console.log("x", x);
  return <ReactPlayer url={x.video_link} controls={true} />;
};

class StoragePage extends Component {
  constructor(props) {
    super(props);

    this.addActiveClassOnImages = this.addActiveClassOnImages.bind(this);
    this.addActiveClassOnVideos = this.addActiveClassOnVideos.bind(this);
    this.addActiveClassOnDocs = this.addActiveClassOnDocs.bind(this);
    this.addActiveClassOnAudios = this.addActiveClassOnAudios.bind(this);

    this.state = {
      images: false,
      videos: false,
      docs: false,
      audios: false,
      showImageModal: false,
      showVideoModal: false,
      showDocModal: false,
      showAudioModal: false,
    };
  }

  componentDidMount() {
    // this.props.fetchWhoAmi();
    // const user_id = await this.props.auth.user.user_id;
    // const url = await this.props.dispatch(fetchVideos(user_id));

    // this.props.dispatch(fetchVideos(user_id));
    console.log("comdidmount");
  }

  addActiveClassOnImages() {
    this.setState({ images: true, videos: false, docs: false, audios: false });
  }

  addActiveClassOnVideos() {
    this.setState({ videos: true, images: false, docs: false, audios: false });
  }

  addActiveClassOnDocs() {
    this.setState({ docs: true, images: false, videos: false, audios: false });
  }

  addActiveClassOnAudios() {
    this.setState({ audios: true, docs: false, images: false, videos: false });
  }

  componentWillMount() {
    //
    //debug
    console.log("comwilmount");
    const user_id = this.props.auth.user.user_id;
    this.props.fetchVideos(user_id);
    this.props.fetchImages(user_id);
    this.props.fetchStorageDocs(user_id);
    this.props.fetchStorageAudios(user_id);
  }

  //show modal
  showImageModal = () => {
    this.setState({ showImageModal: true });
  };

  showVideoModal = () => {
    this.setState({ showVideoModal: true });
  };

  showDocModal = () => {
    this.setState({ showDocModal: true });
  };

  showAudioModal = () => {
    this.setState({ showAudioModal: true });
  };

  onHide = () => {
    this.setState({
      showDocModal: false,
      showImageModal: false,
      showVideoModal: false,
      showAudioModal: false,
    });
  };

  NewlineText = () => {
    const text = this.props.whoami.bio;
    return text.split("\n").map((str) => <p className="m-0 p-0 f-14">{str}</p>);
  };

  render() {
    const {
      whoami,
      fetchvideos,
      fetchimages,
      auth,
      fetchstoragedocs,
      fetchstorageaudios,
    } = this.props;
    const {
      images,
      videos,
      docs,
      audios,
      showAudioModal,
      showVideoModal,
      showDocModal,
      showImageModal,
    } = this.state;

    const no = fetchvideos;

    // console.log("this.props", this.props.children);

    // console.log("render\n", "no:\n", no);
    // console.log("window\n", window);
    // console.log("this.props:\n", this.props);
    // console.log("this.videoRef", this.videoRef);
    // console.log("this.imagesRef", this.imagesRef.current);
    // console.log("this.docsRef", this.docsRef.current);
    // if (fetchvideos.length !== undefined) {

    const userAva = !whoami.avatar ? userNoPict : whoami.avatar;
    let Fullname =
      // whoami.firstname === undefined ||
      // whoami.lastname === undefined ||
      whoami.firstname === null && whoami.lastname === null
        ? `${whoami.username}${whoami.user_id}`
        : `${whoami.firstname} ${whoami.lastname}`;

    console.log("nanCheck:", isNaN(Fullname));

    return (
      <div
        className={
          showDocModal || showImageModal || showVideoModal || showAudioModal
            ? "storage-page sbox-filter-blur"
            : "storage-page"
        }
      >
        <Navbar />
        <Router>
          <Container className="section-main storage-page p-0">
            <Row className="justify-content-center stor-page-first-row">
              <div className="col-lg-2">
                {whoami === undefined ? (
                  <ImageProfileLoader />
                ) : (
                  <img
                    src={userAva}
                    style={{ borderRadius: "100%" }}
                    height="160px"
                    className="avatar-large"
                    alt={`Profile of ${Fullname}`}
                  />
                )}
              </div>
              <div className="col-lg-4 align-items-center position-relative">
                <div className="row">
                  <div className="position-absolute top-50 start-50 translate-middle">
                    <h2 className="font-weight-bold h4 mt-3">
                      {whoami.username === undefined ? (
                        <NameProfileLoader />
                      ) : (
                        `${Fullname}`
                      )}
                    </h2>
                    <div className="mt-3">
                      {whoami === undefined ? (
                        <BioProfileLoader />
                      ) : (
                        <NewlineText bio={whoami.bio} />
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-lg-2">
                <StorageAddButton
                  showImageModal={(e) => this.showImageModal(e)}
                  showVideoModal={(e) => this.showVideoModal(e)}
                  showAudioModal={(e) => this.showAudioModal(e)}
                  showDocModal={(e) => this.showDocModal(e)}
                />
              </div>
            </Row>
            <div className="row stor-page-second-row">
              <Col>
                <ul className="nav nav-tabs nav-fill mb-5">
                  <li className="nav-item">
                    <Link
                      className={
                        images
                          ? "nav-link active text-bold"
                          : "nav-link text-muted"
                      }
                      onClick={this.addActiveClassOnImages}
                      to={`/accounts/storage/images`}
                    >
                      <div></div>
                      <span className="pr-2">
                        <svg
                          width="20"
                          height="20"
                          viewBox="0 0 20 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M17 1H3C1.89543 1 1 1.89543 1 3V17C1 18.1046 1.89543 19 3 19H17C18.1046 19 19 18.1046 19 17V3C19 1.89543 18.1046 1 17 1Z"
                            stroke={images ? "black" : "#8e8e8e"}
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                          <path
                            d="M19 13L14 8L3 19"
                            stroke={images ? "black" : "#8e8e8e"}
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                          <path
                            d="M6.5 8C7.32843 8 8 7.32843 8 6.5C8 5.67157 7.32843 5 6.5 5C5.67157 5 5 5.67157 5 6.5C5 7.32843 5.67157 8 6.5 8Z"
                            stroke={images ? "black" : "#8e8e8e"}
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                        </svg>
                      </span>
                      <span
                        className={
                          `font-weight-bold text-uppercase f-10` +
                          (images
                            ? ` text-black p-absolute-storage-nav-clicked`
                            : ` text-muted p-absolute-storage-nav`)
                        }
                        style={{ position: "absolute" }}
                      >
                        Images
                      </span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      className={
                        videos
                          ? "nav-link active text-bold"
                          : "nav-link text-muted"
                      }
                      onClick={this.addActiveClassOnVideos}
                      to={`/accounts/storage/videos`}
                    >
                      <span className="pr-2">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M23 7L16 12L23 17V7Z"
                            stroke={videos ? "black" : "#8e8e8e"}
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                          <path
                            d="M14 5H3C1.89543 5 1 5.89543 1 7V17C1 18.1046 1.89543 19 3 19H14C15.1046 19 16 18.1046 16 17V7C16 5.89543 15.1046 5 14 5Z"
                            stroke={videos ? "black" : "#8e8e8e"}
                            stroke-width="2"
                            stroke-linecap="round"
                            stroke-linejoin="round"
                          />
                        </svg>
                      </span>
                      <span
                        className={
                          `font-weight-bold text-uppercase f-10` +
                          (videos
                            ? ` text-black p-absolute-storage-nav-clicked`
                            : ` text-muted p-absolute-storage-nav`)
                        }
                        style={{ position: "absolute" }}
                      >
                        Videos
                      </span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      className={
                        docs
                          ? "nav-link active text-bold"
                          : "nav-link text-muted"
                      }
                      onClick={this.addActiveClassOnDocs}
                      to={`/accounts/storage/docs`}
                    >
                      <span className="pr-2">
                        <svg
                          width="16"
                          height="20"
                          viewBox="0 0 16 20"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M16 6.94C15.9896 6.84813 15.9695 6.75763 15.94 6.67V6.58C15.8919 6.47718 15.8278 6.38267 15.75 6.3L9.75 0.3C9.66734 0.222216 9.57282 0.158081 9.47 0.11H9.38L9.06 0H3C2.20435 0 1.44129 0.316071 0.87868 0.87868C0.316071 1.44129 0 2.20435 0 3V17C0 17.7956 0.316071 18.5587 0.87868 19.1213C1.44129 19.6839 2.20435 20 3 20H13C13.7956 20 14.5587 19.6839 15.1213 19.1213C15.6839 18.5587 16 17.7956 16 17V7C16 7 16 7 16 6.94ZM10 3.41L12.59 6H10V3.41ZM14 17C14 17.2652 13.8946 17.5196 13.7071 17.7071C13.5196 17.8946 13.2652 18 13 18H3C2.73478 18 2.48043 17.8946 2.29289 17.7071C2.10536 17.5196 2 17.2652 2 17V3C2 2.73478 2.10536 2.48043 2.29289 2.29289C2.48043 2.10536 2.73478 2 3 2H8V7C8 7.26522 8.10536 7.51957 8.29289 7.70711C8.48043 7.89464 8.73478 8 9 8H14V17Z"
                            fill={docs ? "black" : "#8e8e8e"}
                          />
                        </svg>
                      </span>
                      <span
                        className={
                          `font-weight-bold text-uppercase f-10` +
                          (docs
                            ? ` text-black p-absolute-storage-nav-clicked`
                            : ` text-muted p-absolute-storage-nav`)
                        }
                        style={{ position: "absolute" }}
                      >
                        Docs
                      </span>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link
                      className={
                        audios
                          ? "nav-link active text-bold"
                          : "nav-link text-muted"
                      }
                      onClick={this.addActiveClassOnAudios}
                      to={`/accounts/storage/audios`}
                    >
                      <span className="pr-2">
                        <svg
                          width="24"
                          height="24"
                          viewBox="0 0 24 24"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M21.65 2.24003C21.541 2.14648 21.4131 2.07752 21.2751 2.03783C21.137 1.99814 20.992 1.98866 20.85 2.01003L7.85 4.01003C7.61326 4.04595 7.39727 4.16562 7.24129 4.34729C7.0853 4.52897 6.99969 4.76058 7 5.00003V15.35C6.53277 15.1218 6.01999 15.0022 5.5 15C4.80777 15 4.13108 15.2053 3.55551 15.5899C2.97993 15.9745 2.53133 16.5211 2.26642 17.1606C2.00152 17.8002 1.9322 18.5039 2.06725 19.1828C2.2023 19.8618 2.53564 20.4854 3.02513 20.9749C3.51461 21.4644 4.13825 21.7977 4.81719 21.9328C5.49612 22.0678 6.19985 21.9985 6.83939 21.7336C7.47894 21.4687 8.02556 21.0201 8.41015 20.4445C8.79473 19.869 9 19.1923 9 18.5V10.86L20 9.17003V13.35C19.5328 13.1218 19.02 13.0022 18.5 13C17.8078 13 17.1311 13.2053 16.5555 13.5899C15.9799 13.9745 15.5313 14.5211 15.2664 15.1606C15.0015 15.8002 14.9322 16.5039 15.0673 17.1828C15.2023 17.8618 15.5356 18.4854 16.0251 18.9749C16.5146 19.4644 17.1383 19.7977 17.8172 19.9328C18.4961 20.0678 19.1999 19.9985 19.8394 19.7336C20.4789 19.4687 21.0256 19.0201 21.4101 18.4445C21.7947 17.869 22 17.1923 22 16.5V3.00003C22 2.85559 21.9687 2.71286 21.9083 2.58166C21.8479 2.45047 21.7598 2.33391 21.65 2.24003ZM5.5 20C5.20333 20 4.91332 19.9121 4.66665 19.7472C4.41997 19.5824 4.22771 19.3481 4.11418 19.0741C4.00065 18.8 3.97095 18.4984 4.02882 18.2074C4.0867 17.9164 4.22956 17.6491 4.43934 17.4394C4.64912 17.2296 4.91639 17.0867 5.20737 17.0289C5.49834 16.971 5.79994 17.0007 6.07403 17.1142C6.34812 17.2277 6.58238 17.42 6.74721 17.6667C6.91203 17.9133 7 18.2034 7 18.5C7 18.8979 6.84197 19.2794 6.56066 19.5607C6.27936 19.842 5.89783 20 5.5 20ZM18.5 18C18.2033 18 17.9133 17.9121 17.6666 17.7472C17.42 17.5824 17.2277 17.3481 17.1142 17.0741C17.0007 16.8 16.9709 16.4984 17.0288 16.2074C17.0867 15.9164 17.2296 15.6491 17.4393 15.4394C17.6491 15.2296 17.9164 15.0867 18.2074 15.0289C18.4983 14.971 18.7999 15.0007 19.074 15.1142C19.3481 15.2277 19.5824 15.42 19.7472 15.6667C19.912 15.9133 20 16.2034 20 16.5C20 16.8979 19.842 17.2794 19.5607 17.5607C19.2794 17.842 18.8978 18 18.5 18ZM20 7.14003L9 8.83003V5.83003L20 4.17003V7.14003Z"
                            fill={audios ? "black" : "#8e8e8e"}
                          />
                        </svg>
                      </span>
                      <span
                        className={
                          `font-weight-bold text-uppercase f-10` +
                          (audios
                            ? ` text-black p-absolute-storage-nav-clicked`
                            : ` text-muted p-absolute-storage-nav`)
                        }
                        style={{ position: "absolute" }}
                      >
                        Audios
                      </span>
                    </Link>
                  </li>
                </ul>

                <Switch>
                  <Route path={`/accounts/storage/images`}>
                    <ImagesList fetchimages={fetchimages.fetchimages} />
                  </Route>
                  <Route path={`/accounts/storage/videos`}>
                    <VideosList
                      fetchvideos={fetchvideos.fetchvideos}
                      ref={this.videoRef}
                    />
                  </Route>
                  <Route path={`/accounts/storage/docs`}>
                    <DocsList fetchdocs={fetchstoragedocs} />
                  </Route>
                  <Route path={`/accounts/storage/audios`}>
                    <AudiosList fetchdocs={fetchstorageaudios} />
                  </Route>
                </Switch>
              </Col>
            </div>
            <div className="row justify-content-center">
              <Switch>
                <Route exact path={`/acccounts/storage/`}>
                  <div className="text-center" style={{ paddingTop: "5rem" }}>
                    You Can Add Image Video and Docs
                  </div>
                </Route>
              </Switch>
            </div>
            <AddImageModal
              show={showImageModal}
              onHide={this.onHide}
              user_id={auth.user.user_id}
            />
            <AddVideoModal
              show={showVideoModal}
              onHide={this.onHide}
              user_id={auth.user.user_id}
            />
            <AddAudioModal
              show={showAudioModal}
              onHide={this.onHide}
              user_id={auth.user.user_id}
            />
            <AddDocModal
              show={showDocModal}
              onHide={this.onHide}
              user_id={auth.user.user_id}
            />
          </Container>
        </Router>
      </div>
    );
  }
}

// const NewlineText = () => {
//   const text = this.props.whoami.bio;
//   return text.split("\n").map((str) => <p className="m-0 p-0 f-12">{str}</p>);
// };

const ImageProfileLoader = () => (
  <ContentLoader viewBox="0 0 140 140">
    <circle cx="60" cy="60" r="56" />
  </ContentLoader>
);

const NameProfileLoader = () => (
  <ContentLoader viewBox="0 0 236 30">
    <rect x="0" y="0" rx="3" ry="3" width="230" height="28" />
  </ContentLoader>
);

const BioProfileLoader = () => (
  <ContentLoader viewBox="0 0 300 160">
    <rect x="0" y="0" rx="3" ry="3" width="300" height="20" />
    <rect x="0" y="30" rx="3" ry="3" width="100" height="20" />
    <rect x="112" y="30" rx="3" ry="3" width="190" height="20" />
    <rect x="0" y="60" rx="3" ry="3" width="300" height="20" />
    <rect x="0" y="90" rx="3" ry="3" width="150" height="20" />
  </ContentLoader>
);

StoragePage.propTypes = {
  auth: PropTypes.object.isRequired,
  whoami: PropTypes.object.isRequired,
  fetchVideos: PropTypes.func.isRequired,
  fetchImages: PropTypes.func.isRequired,
  fetchStorageDocs: PropTypes.func.isRequired,
  fetchStorageAudios: PropTypes.func.isRequired,
  fetchstorageaudios: PropTypes.object.isRequired,
  fetchstoragedocs: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  whoami: state.whoami.whoami,
  fetchvideos: state.fetchvideos,
  fetchimages: state.fetchimages,
  fetchstoragedocs: state.fetchstoragedocs.fetchstoragedocs,
  fetchstorageaudios: state.fetchstorageaudios.fetchstorageaudios,
});

export default connect(mapStateToProps, {
  fetchStorageDocs,
  fetchVideos,
  fetchImages,
  fetchStorageAudios,
})(StoragePage);
