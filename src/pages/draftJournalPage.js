import React, { Component } from "react";
import { JournalsPrivate } from "../components/UserJournalsPrivate";
import {
  BrowserRouter as Router,
  withRouter,
  Switch,
  Route,
} from "react-router-dom";
import { connect } from "react-redux";
import Navbar from "../components/navbar/navbar";
import PropTypes from "prop-types";
import { Container } from "react-bootstrap";
import { fetchPrivateJournals } from "../redux/actions/fetchPrivateJournalsUserAction";

class DrafJournalPage extends Component {
  componentDidMount() {
    this.props.fetchPrivateJournals(this.props.auth.user.user_id);
  }

  render() {
    const { match, loading, privatejournalssuser, whoami } = this.props;
    //
    //debug
    console.log("this.props:", this.props);
    console.log("match", match);
    console.log("privatejournalsusers:", privatejournalssuser);
    return (
      <>
        <div className="public-user-page">
          <Navbar />
          <Router>
            <Container className="section-main storage-page p-0">
              <Switch>
                <Route exact path={`/accounts/journals_draft`}>
                  <div className="row justify-content-center">
                    <div className="col-lg-public-user-page col-md-6 col-sm-12">
                      <JournalsPrivate
                        privatejournalssuser={privatejournalssuser}
                        whoamiuserid={whoami.user_id}
                        loading={loading}
                      />
                    </div>
                  </div>
                </Route>
              </Switch>
            </Container>
          </Router>
        </div>
      </>
    );
  }
}

DrafJournalPage.propTypes = {
  auth: PropTypes.object.isRequired,
  whoami: PropTypes.object.isRequired,
  privatejournalssuser: PropTypes.object.isRequired,
  fetchPrivateJournals: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  privatejournalssuser: state.fetchprivatejournals.fetchprivatejournals,
  loading: state.fetchprivatejournals.loading,
  whoami: state.whoami.whoami,
  auth: state.auth,
});

export default connect(mapStateToProps, { fetchPrivateJournals })(
  withRouter(DrafJournalPage)
);
