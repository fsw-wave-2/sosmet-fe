import React, { Component } from "react";
import { InfoWeb } from "../components/sidebar/SidebarFooter";
import { SuggestedAccounts } from "../components/sidebar/SidebarSuggestedAccounts";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { fetchPublicUsers } from "../redux/actions/publicUsers";
import { fetchPublicPosts } from "../redux/actions/publicPosts";
import { fetchWhoAmi } from "../redux/actions/whoAmiAction";
import { fetchRandomUsers } from "../redux/actions/randomUsersAction";
import SidebarProfileOverview from "../components/sidebar/sidebarProfileOverview";
import Navbar from "../components/navbar/navbar";
import { Feeds } from "../components/post/PublicFeeds";
import "react-toastify/dist/ReactToastify.css";
import MobileNavbar from "../components/navbar/MobileNavbar";

class Landing extends Component {
  constructor(props) {
    super(props);
    //debug
    // console.log("TEST" + usersPublic);
    // this.state = { matches: window.matchMedia("(min-width: 768px)").matches };
  }

  componentDidUpdate(prevProps) {
    if (prevProps.postText !== this.props.postText) {
      this.props.fetchPublicPosts();
      // this.props.dispatch(postStart());
      //
      //debug
      // console.log("componentDidUpdate(prevProps):", true);
    }
    //
    //debug
    // console.log("componentDidUpdate(prevProps):\n", prevProps.postText);
    // console.log("componentDidUpdate(this.props):\n", this.props.postText);
    // console.log("componentDidUpdate(this.props.postText):\n", this.props);
  }

  componentWillMount() {
    //
    //debug
    // console.log("compWillMount:");

    // this.props.dispatch(setCurrentUser());
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/home");
    }
  }

  componentDidMount() {
    //
    //debug
    // console.log(this.props.errorposts);

    // const handler = (e) => this.setState({ matches: e.matches });
    // window.matchMedia("(min-width: 768px)").addListener(handler);
    this.props.fetchPublicPosts();
    // this.props.fetchPublicUsers();
    this.props.fetchRandomUsers();

    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/home");
    } else {
      this.props.history.push("/");
    }
  }

  render() {
    const {
      publicposts,
      linksInfoWeb,
      auth,
      whoami,
      // publicusers,
      postText,
      postdetails,
      loves,
      commentspost,
      loginmodal,
      registermodal,
      randomusers,
    } = this.props;

    // const post_id = publicusers.find((x) => post_id);
    // const spliturl = window.location.pathname.split("/");
    // const postIdStr = spliturl[3];
    // const data = publicusers;
    // const x = data.find((x) => x.post_id === postIdStr);

    //
    //debug
    // console.log("publicusers:", publicusers);
    console.log("randomusers:", randomusers);
    // console.log("error:", error);
    // console.log("WHOAMIIIIII:\n", whoami);
    // console.log("postTEXTTTT:\n", postText);
    // console.log("AUTHHHHH:", auth);
    // console.log("x", x);
    // console.log("postdetails:", postdetails);
    // console.log(" commentspost:", commentspost);
    // console.log(" loves:", loves);
    // console.log("this.props", this.props);
    // console.log("clientAgent:", window.clientInformation);
    // console.log("loginmodal", loginmodal);
    return (
      <div
        className={
          !loginmodal.showLogin && !registermodal.showRegister
            ? "landing-page"
            : "landing-page sbox-filter-blur"
        }
      >
        {/* {this.state.matches && <Navbar />}
        {!this.state.matches && <MobileNavbar />} */}
        <Navbar />
        <div
          // className={
          //   !this.state.matches
          //     ? "section-main-mobile container"
          //     : "section-main container"
          // }
          className="section-main container"
        >
          <div className="row">
            <div className="feeds-wrapper col-lg-8 col-md-8 col-sm-12">
              <div className="container">
                <Feeds
                  publicposts={publicposts}
                  whoami={whoami}
                  postText={postText}
                  auth={auth}
                  loves={loves}
                  postdetails={postdetails}
                  commentspost={commentspost}
                ></Feeds>
              </div>
            </div>

            <aside className="sidebar-wrapper col-4">
              <div className="sticky-wrapper-aside">
                <div className="overflow-wrapper-aside">
                  <SidebarProfileOverview auth={auth} whoami={whoami} />
                  <div className="sidebar-suggested-account mt-3">
                    <div className="h6 mb-2">Suggested Account</div>
                    <div>
                      <SuggestedAccounts
                        randomusers={randomusers}
                      ></SuggestedAccounts>
                    </div>
                    <div className="mt-3">
                      <a href="/suggested" className="more-account-link">
                        <span className="more-account pr-2">more</span>
                        <svg
                          width="8"
                          height="6"
                          viewBox="0 0 8 6"
                          fill="none"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            d="M4 6L0.535899 -3.01142e-07L7.4641 -9.06825e-07L4 6Z"
                            fill="black"
                          />
                        </svg>
                      </a>
                    </div>
                  </div>
                  <InfoWeb linksInfoWeb={linksInfoWeb}></InfoWeb>
                </div>
              </div>
            </aside>
          </div>
        </div>
      </div>
    );
  }
}

Landing.propsTypes = {
  auth: PropTypes.object.isRequired,
  fetchPublicPosts: PropTypes.func.isRequired,
  // fetchPublicUsers: PropTypes.func.isRequired,
  fetchRandomUsers: PropTypes.func.isRequired,
  fetchWhoAmi: PropTypes.func.isRequired,
  publicposts: PropTypes.object.isRequired,
  // publicusers: PropTypes.object.isRequired,
  loginmodal: PropTypes.object.isRequired,
  registermodal: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  randomusers: state.randomusers.randomusers,
  publicposts: state.publicposts.publicposts,
  loadingrandomusers: state.randomusers.loading,
  loadingposts: state.publicposts.loading,
  linksInfoWeb: state.linksInfoWeb,
  whoami: state.whoami.whoami,
  auth: state.auth,
  postText: state.postText,
  loves: state.loves,
  commentspost: state.commentspost,
  postdetails: state.postdetails,
  loginmodal: state.loginmodal,
  registermodal: state.registermodal,
});

export default connect(mapStateToProps, {
  fetchRandomUsers,
  fetchPublicPosts,
  fetchWhoAmi,
})(Landing);
