import React, { Component } from "react";
import { connect } from "react-redux";
import { SuggestedAccounts } from "../components/SuggestedAccounts";
import Navbar from "../components/navbar/navbar";
import { Container } from "react-bootstrap";
import { fetchPublicUsers } from "../redux/actions/publicUsers";
import PropTypes from "prop-types";
import { useParams } from "react-router-dom";

class SuggestedPage extends Component {
  componentDidMount() {
    console.log("componentDidMount");
    console.log("this.props:", this.props);
    console.log("this.props.match:", this.props.match);
    this.props.fetchPublicUsers();
  }

  render() {
    const { publicusers, loading } = this.props;

    console.log("loading", loading);
    return (
      <>
        <Navbar />
        <Container className="section-main">
          <div className="suggested-page">
            <div className="section-main container">
              <div className="row justify-content-center">
                <div className="col-4">
                  <div className="mb-4">
                    <h3 className="h5 font-weight-bold">Suggested Users</h3>
                  </div>
                  <div className="suggested-wrapper">
                    <Suggested publicusers={publicusers} />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Container>
      </>
    );
  }
}

const Suggested = ({ publicusers }) => {
  let { suggested } = useParams();

  // console.log(suggested);

  return (
    <>
      <h2>{suggested}</h2>
      <SuggestedAccounts publicusers={publicusers} />
    </>
  );
};

SuggestedPage.propsTypes = {
  fetchPublicUsers: PropTypes.func.isRequired,
  publicusers: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  publicusers: state.publicusers.publicusers,
  loading: state.publicusers.loading,
});

export default connect(mapStateToProps, { fetchPublicUsers })(SuggestedPage);
