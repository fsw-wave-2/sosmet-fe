import React, { Component } from "react";
import { connect } from "react-redux";
import { Container, Row, Col } from "react-bootstrap";
import SidebarProfileOverview from "../components/sidebar/sidebarProfileOverview";
import { PostDetailsDescription } from "../components/post/postDetailDescriptions";
import PropTypes from "prop-types";
import { SuggestedAccounts } from "../components/sidebar/SidebarSuggestedAccounts";
import { InfoWeb } from "../components/sidebar/SidebarFooter";
import {
  fetchPostLoves,
  fetchPostLovesSuccess,
} from "../redux/actions/publicPostLovesAction";
import { fetchCommentsPost } from "../redux/actions/fetchCommentsPostAction";
import Comments from "../components/post/Comments";
import Navbar from "../components/navbar/navbar";
import { fetchPostDetails } from "../redux/actions/postDetailsAction";
import { BrowserRouter as Router } from "react-router-dom";
import { fetchRandomUsers } from "../redux/actions/randomUsersAction";

class PostDetails extends Component {
  componentDidUpdate(prevProps, prevState) {
    console.log(
      "componentDidUpdate(prevProps.loves.loading):\n",
      prevProps.loves.loading
    );
    console.log(
      "componentDidUpdate(this.props.loves.loading):\n",
      this.props.loves.loading
    );
    if (prevProps.loves.loading !== this.props.loves.loading) {
      const postId = this.props.match.params.id;
      this.props.fetchPostLoves(postId);
    }
  }

  // async componentWillMount() {
  //   //
  //   //debug
  //   console.log("componentWillMount:");
  //   const postIdStr = await this.props.match.params.id;
  //   const postIdInt = parseInt(postIdStr);

  //   await this.props.fetchPostDetails(postIdInt);
  //   await this.props.fetchPostLoves(postIdInt);
  //   await this.props.fetchCommentsPost(postIdInt);
  // }

  componentDidMount() {
    window.scrollTo(0, 0);
    // console.log("PostDetails:");
    // console.log("window:", window);
    // console.log("this.props.params:", this.props.params);
    console.log("this.props.match:", this.props.match);

    const postIdStr = this.props.match.params.id;
    const postIdInt = parseInt(postIdStr);

    this.props.fetchPostDetails(postIdInt);
    this.props.fetchPostLoves(postIdInt);
    this.props.fetchCommentsPost(postIdInt);
    this.props.fetchRandomUsers();
  }

  render() {
    const {
      postdetails,
      whoami,
      auth,
      linksInfoWeb,
      loves,
      match,
      commentspost,
      randomusers,
    } = this.props;

    const pathname = window.location.pathname;
    const hostname = window.location.hostname;

    //
    //debug
    // console.log("match.params.id:", match.params);
    console.log("postdetails:", postdetails);
    // console.log("commentspost:", commentspost);
    // console.log("url:", url.href);
    // console.log(this.props.match);
    // console.log("loves", loves);
    // console.log("postId:", postIdStr);
    // console.log("window:", window);
    // console.log("x:", x);
    // console.log("data:", data);
    console.log("whoami:", whoami);
    // console.log("postdetails.user.username:", postdetails.user.username);

    return (
      <>
        <Navbar />
        <Router>
          <Container className="section-main">
            <Row>
              <Col lg={8} className="feeds-wrapper pl-0 pr-5">
                <div className="heading-profile-page-wrapper">{}</div>
                <div className="feeds-whoami-page">
                  <div className="feeds-whoami-page-wrapper">
                    <div className="feeds-whoami-page-overflow">
                      <PostDetailsDescription
                        postdetails={postdetails}
                        pathname={pathname}
                        hostname={hostname}
                        whoami={whoami}
                        loves={loves.loves}
                        commentspost={commentspost.commentspost}
                      ></PostDetailsDescription>
                      <Comments
                        commentspost={commentspost.commentspost}
                        postId={match.params.id}
                      />
                    </div>
                  </div>
                </div>
              </Col>
              <Col lg={4} className="sidebar-wrapper">
                <SidebarProfileOverview auth={auth} whoami={whoami} />
                <div className="sidebar-suggested-account mt-3">
                  <div className="h6 mb-2">Suggested Account</div>
                  <div>
                    <SuggestedAccounts
                      randomusers={randomusers}
                    ></SuggestedAccounts>
                  </div>
                  <div className="mt-3">
                    <a href="/suggested" className="more-account-link">
                      <span className="more-account pr-2">more</span>
                      <svg
                        width="8"
                        height="6"
                        viewBox="0 0 8 6"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          d="M4 6L0.535899 -3.01142e-07L7.4641 -9.06825e-07L4 6Z"
                          fill="black"
                        />
                      </svg>
                    </a>
                  </div>
                </div>
                <InfoWeb linksInfoWeb={linksInfoWeb}></InfoWeb>
              </Col>
            </Row>
          </Container>
        </Router>
      </>
    );
  }
}

PostDetails.propsTypes = {
  auth: PropTypes.object.isRequired,
  postdetails: PropTypes.object.isRequired,
  whoami: PropTypes.object.isRequired,
  fetchPostLoves: PropTypes.func.isRequired,
  fetchCommentsPost: PropTypes.func.isRequired,
  fetchPostLovesSuccess: PropTypes.func.isRequired,
  fetchPostDetails: PropTypes.func.isRequired,
  linksInfoWeb: PropTypes.object.isRequired,
  loves: PropTypes.object.isRequired,
  commentspost: PropTypes.object.isRequired,
  fetchRandomUsers: PropTypes.func.isRequired,
  randomusers: PropTypes.object.isRequired,
};
const mapStateToProps = (state) => ({
  postdetails: state.postdetails.postdetails,
  whoami: state.whoami.whoami,
  auth: state.auth,
  loves: state.loves,
  linksInfoWeb: state.linksInfoWeb,
  commentspost: state.commentspost,
  randomusers: state.randomusers.randomusers,
});

export default connect(mapStateToProps, {
  fetchPostDetails,
  fetchCommentsPost,
  fetchPostLoves,
  fetchRandomUsers,
  fetchPostLovesSuccess,
})(PostDetails);
