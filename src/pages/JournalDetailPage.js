import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchPublicJournal } from "../redux/actions/fetchPublicJournalUserAction";
import Navbar from "../components/navbar/navbar";
import PropTypes from "prop-types";
import { BrowserRouter as Router } from "react-router-dom";
import { JournalsPublic } from "../components/UserJournalsPublic";
import { ContentJournalSection } from "../components/ContentJournalSection";
import ReactHtmlParser from "react-html-parser";

class JournalDetailsPage extends Component {
  componentDidMount() {
    const journal_id = this.props.match.params.id;

    this.props.fetchPublicJournal(journal_id);

    //
    //debug
    console.log("journal_id:", journal_id);
  }

  render() {
    const { publicjournaldetail, match, loading } = this.props;
    //
    //
    console.log("fetchpublicjournal:", publicjournaldetail);
    console.log("this.props", this.props);
    return (
      <>
        <Navbar />
        <Router>
          <div className="container section-main">
            <div
              className="row justify-content-between"
              style={{ marginTop: "6rem" }}
            >
              <div className="col-sm-12">
                <div className="journal-page-detail-main">
                  <ContentJournalSection
                    publicjournaldetail={publicjournaldetail}
                    loading={loading}
                  />
                </div>
              </div>
            </div>
          </div>
        </Router>
      </>
    );
  }
}

JournalDetailsPage.propType = {
  fetchPublicJournal: PropTypes.func.isRequired,
  publicjournaldetail: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
  publicjournaldetail: state.fetchpublicjournal.fetchpublicjournal,
  loading: state.fetchpublicjournal.loading,
});

export default connect(mapStateToProps, { fetchPublicJournal })(
  JournalDetailsPage
);
