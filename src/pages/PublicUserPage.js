import React, { Component } from "react";
import { connect } from "react-redux";
import Navbar from "../components/navbar/navbar";
import { Container } from "react-bootstrap";
import PropTypes from "prop-types";
import { BrowserRouter as Router } from "react-router-dom";
import PublicUserDetail from "../components/profile/ProfilePublicUserDetail";
import { searchUser } from "../redux/actions/searchUserAction";
import AddPost from "../components/modals/AddPost";
import UsersServices from "../services/usersService";

class PublicUserPage extends Component {
  state = {
    show: false,
    postArray: [],
    postArrayLength: 0,
  };

  async componentDidMount() {
    //
    //debug
    // console.log("componentDidMount");
    // console.log("this.props:", this.props);
    // console.log("this.props.match:", this.props.match.params.username);
    // console.log("this.props.match.params:", this.props.params);
    // this.props.fetchPublicUsers();
    const username = this.props.match.params.username;

    await this.props.searchUser(username);
  }

  onSumbitJournal = async () => {
    const token = await localStorage.jwtToken;
    const payload_create_journal = {
      user_id: this.props.whoami.user_id,
    };
    await UsersServices.createJournal(token, payload_create_journal)
      .then((res) => {
        //
        //debug
        // console.log("res", res);
        const journal_id = res.data.journal_id;
        this.props.history.push(`/accounts/journals_editor/${journal_id}`);
      })
      .catch((err) => {
        //
        //debug
        console.log("err", err);
      });
  };

  handleShow = () => {
    //
    //debug
    // console.log(this.state.postArray);
    this.setState({ show: true });
  };

  // closing modal
  handleClose = () => {
    this.setState({ show: false });
    //
    //debug
    // console.log(this.state.postArray);
  };

  render() {
    const { searchuser, location, match, whoami, auth } = this.props;
    //
    //debug
    // console.log("searchuser:", searchuser);
    // console.log("match:", match);
    // console.log("location:", location);

    return (
      <>
        <div className="public-user-page">
          <Navbar />
          <Router>
            <Container className="section-main storage-page p-0">
              <PublicUserDetail
                searchuser={searchuser}
                whoami={whoami}
                auth={auth}
                matchwhoami={match.params.username}
                handleShow={(e) => this.handleShow(e)}
                onClick={this.onSumbitJournal}
              />
            </Container>
          </Router>
        </div>
        <AddPost show={this.state.show} handleClose={this.handleClose} />
      </>
    );
  }
}

PublicUserPage.propsTypes = {
  auth: PropTypes.object.isRequired,
  whoami: PropTypes.object.isRequired,
  searchuser: PropTypes.object.isRequired,
  searchUser: PropTypes.func.isRequired,
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  whoami: state.whoami.whoami,
  searchuser: state.searchuser.searchuser[0],
});

export default connect(mapStateToProps, { searchUser })(PublicUserPage);
