import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { fetchStorageVideoDetails } from "../redux/actions/fetchStorageVideoDetails";
import { BrowserRouter as Router } from "react-router-dom";
import Navbar from "../components/navbar/navbar";
import { VideoDetailsDescription } from "../components/videoDetailsDescription";

class StorageVideoDetailsPage extends Component {
  componentDidMount() {
    //
    //debug
    console.log("this.props.match:", this.props.match.params.id);
    window.scrollTo(0, 0);

    this.props.fetchStorageVideoDetails(this.props.match.params.id);
  }
  render() {
    const { auth, whoami, loading, storagevideodetails } = this.props;

    return (
      <>
        <Navbar />
        <Router>
          <div className="section-main container">
            <div className="row justify-content-center">
              <div className="storage-video-detail-page">
                <div className="col-lg-11 col-md-8 col-sm m-auto">
                  <VideoDetailsDescription
                    storagevideodetails={storagevideodetails}
                  />
                </div>
              </div>
            </div>
          </div>
        </Router>
      </>
    );
  }
}

StorageVideoDetailsPage.propTypes = {
  auth: PropTypes.object.isRequired,
  whoami: PropTypes.object.isRequired,
  storagevideodetails: PropTypes.object.isRequired,
  fetchStorageVideoDetails: PropTypes.func.isRequired,
  loading: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  storagevideodetails: state.storagevideodetails.storagevideodetails,
  loading: state.storagevideodetails.loading,
  auth: state.auth,
  whoami: state.whoami.whoami,
});

export default connect(mapStateToProps, {
  fetchStorageVideoDetails,
})(StorageVideoDetailsPage);
