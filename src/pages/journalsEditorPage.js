import React, { useState, Component } from "react";
import { useParams, withRouter } from "react-router-dom";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import Navbar from "../components/navbar/navbar";
import { fetchPrivateJournal } from "../redux/actions/fetchPrivateJournalUserAction";
import {
  updatePrivateJournal,
  updatePrivateJournalStart,
} from "../redux/actions/updatePrivateJournalAction";
import { convertToHTML, convertFromHTML } from "draft-convert";
import debounce from "lodash/debounce";
import UsersServices from "../services/usersService";

//---**Component**---//
//
class JournalsEditor extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  async componentDidMount() {
    const journal_id = this.props.match.params.id;
    await this.props.fetchPrivateJournal(journal_id);

    const content = await this.props.fetchprivatejournal.fetchprivatejournal
      .content;
    //
    //debug
    console.log("componentDidMount this.props:", this.props);
    console.log(
      "this.props.fetchprivatejournal.fetchprivatejournal.content:",
      this.state
    );
    console.log("content: ", content);
    this.contentRawFetch(content);
    this.props.updatePrivateJournalStart(
      this.props.fetchprivatejournal.fetchprivatejournal
    );
  }

  contentRawFetch = (content_raw) => {
    if (content_raw) {
      this.setState({
        editorState: EditorState.createWithContent(
          convertFromHTML(content_raw)
        ),
      });
    } else {
      this.setState({ editorState: EditorState.createEmpty() });
    }
  };

  saveContent = async (content) => {
    window.localStorage.setItem(
      "content",
      JSON.stringify(convertToRaw(content))
    );
    // const convert_content = await JSON.stringify(convertToRaw(content));
    //
    //debug
    // console.log("content:", content);
    // console.log("converttoraw:", convert_content);
    // const journal_id = this.props.match.params.id;
    // const paylod_update_journal = {
    //   // journal_id: this.props.match.params.id,

    //   content: convert_content,
    // };

    // this.props.updatePrivateJournal(paylod_update_journal, journal_id);
  };

  publishJournal = () => {
    // const raw_content_from_server = this.props.fetchprivatejournal
    //   .fetchprivatejournal.content;
    // const JournalConvertToRaw = convertFromRaw(
    //   JSON.parse(raw_content_from_server)
    // );
    // const contentAsHtml = convertToHTML(JournalConvertToRaw);

    const journal_id = this.props.fetchprivatejournal.fetchprivatejournal
      .journal_id;
    const set_public = {
      set_public: true,
    };
    const token = localStorage.jwtToken;

    UsersServices.updateJournal(token, set_public, journal_id)
      .then((res) => {
        //
        //debug
        console.log("res:", res.data);
      })
      .catch((err) => {
        //
        //debug
        console.log("err:", err.message);
      });

    // console.log("contentAsHtml:", contentAsHtml);
  };

  onChange = (editorState) => {
    const contentState = editorState.getCurrentContent();
    this.saveContent(contentState);
    this.setState({
      editorState,
    });
  };

  onChangeJournalAttribute = (e) => {
    let journaldataupdate = this.props.fetchprivatejournal.fetchprivatejournal;
    if (e.target.name === "title") {
      journaldataupdate.title = e.target.value;
    } else if (e.target.name === "excerpt") {
      journaldataupdate.excerpt = e.target.value;
    } else if (e.target.name === "thumbnail") {
      journaldataupdate.thumbnail = e.target.value;
    }
    this.props.updatePrivateJournalStart(journaldataupdate);
    //
    //debug
    console.log("journaldataupdate", journaldataupdate);
  };

  onSave = () => {
    const content = localStorage.content;
    const JournalConvertToRaw = convertFromRaw(JSON.parse(content));
    const contentAsHtml = convertToHTML(JournalConvertToRaw);
    const journal_id = this.props.match.params.id;
    const paylod_update_journal = {
      title: this.props.fetchprivatejournal.fetchprivatejournal.title,
      thumbnail: this.props.fetchprivatejournal.fetchprivatejournal.thumbnail,
      excerpt: this.props.fetchprivatejournal.fetchprivatejournal.excerpt,
      content: contentAsHtml,
    };

    this.props.updatePrivateJournal(paylod_update_journal, journal_id);

    console.log("content", content);
    console.log("paylod_update_journal", paylod_update_journal);
  };

  render() {
    const { params, fetchprivatejournal, updateprivatejournal } = this.props;

    console.log("this.props:", this.props);
    console.log("params:", params);
    console.log("fetchprivatejournal", fetchprivatejournal);

    return (
      <div className="journals-editor-page">
        <Navbar />
        <div className="section-main">
          <div className="container journals-editor-container">
            <div
              className="feeds-wrapper position-fixed"
              style={{ left: "5rem", right: "5rem" }}
            >
              {!this.state.editorState ? (
                <h3>Loading...</h3>
              ) : (
                <div className="justify-content-between d-flex">
                  <div className="main-journals-editor w-100 p-4">
                    <div className=" mb-3">
                      <textarea
                        className="form-control text-align-center p-3"
                        name="title"
                        type="text"
                        onChange={(e) => this.onChangeJournalAttribute(e)}
                        value={updateprivatejournal.updateprivatejournal.title}
                        autoComplete="off"
                        placeholder="type your journal title here..."
                      />
                    </div>
                    <Editor
                      editorState={this.state.editorState}
                      onEditorStateChange={this.onChange}
                      wrapperClassName="wrapper-class"
                      editorClassName="editor-class"
                      toolbarClassName="toolbar-class"
                    />
                  </div>

                  <div className="sidebar-journal-editor p-4">
                    <div className="mb-3">
                      <div className="excerpt-container mb-3">
                        <textarea
                          className="form-control text-align-center f-12"
                          name="excerpt"
                          type="text"
                          onChange={(e) => this.onChangeJournalAttribute(e)}
                          value={
                            updateprivatejournal.updateprivatejournal.excerpt
                          }
                          as="textarea"
                          autoComplete="off"
                          placeholder="type here excerpt:..."
                        />
                      </div>
                      <div className="image-link-thumbnail-container mb-5">
                        <div className="label f-12">thumbnail link:</div>
                        <textarea
                          className="form-control text-align-center f-12"
                          name="thumbnail"
                          onChange={(e) => this.onChangeJournalAttribute(e)}
                          value={
                            updateprivatejournal.updateprivatejournal.thumbnail
                          }
                          type="text"
                          as="textarea"
                          autoComplete="off"
                          placeholder=""
                        />
                      </div>
                      <div className="d-flex justify-content-between">
                        <button
                          className="btn btn-purple f-10"
                          onClick={this.onSave}
                        >
                          save
                        </button>
                        <button
                          className="btn btn-purple f-10"
                          onClick={this.publishJournal}
                        >
                          publish
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

//---**Hooks**---//
//
// export default function JournalsEditor() {
//   let { id } = useParams();
//   //
//   //debug
//   console.log("id", id);

//   const [editorState, setEditorState] = useState(() =>
//     EditorState.createEmpty()
//   );

//   const handleEditorChange = (state) => {
//     setEditorState(state);
//   };

//   return (
//     <div className="journals-editor-page">
//       <Navbar />
//       <div className="section-main">
//         <div className="container">
//           <div className="feeds-wrapper">
//             <Editor
//               editorState={editorState}
//               onEditorStateChange={handleEditorChange}
//               wrapperClassName="wrapper-class"
//               editorClassName="editor-class"
//               toolbarClassName="toolbar-class"
//               placeholder="Type journals here..."
//             />
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// }

// export default withRouter(JournalsEditor);

JournalsEditor.propTypes = {
  updatePrivateJournal: PropTypes.func.isRequired,
  updatePrivateJournalStart: PropTypes.object.isRequired,
  fetchPrivateJournal: PropTypes.func.isRequired,
  fetchprivatejournal: PropTypes.object.isRequired,
  updateprivatejournal: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  updateprivatejournal: state.updateprivatejournal,
  fetchprivatejournal: state.fetchprivatejournal,
});

export default connect(mapStateToProps, {
  updatePrivateJournalStart,
  fetchPrivateJournal,
  updatePrivateJournal,
})(withRouter(JournalsEditor));
