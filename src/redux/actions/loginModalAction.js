import * as actionTypes from "./actionTypes";

export const loginModalShow = () => ({
  type: actionTypes.LOGIN_MODAL_SHOW,
});

export const loginModalHide = () => ({
  type: actionTypes.LOGIN_MODAL_HIDE,
});
