import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const searchUser = (username) => (dispatch) => {
  const token = localStorage.jwtToken;
  UsersServices.searchUserByUsername(username, token)
    .then((res) => {
      //
      //debug
      console.log("res.data:", res.data);
      dispatch(searchUserSuccess(res.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(searchUserFail(err.message));
    });
};

export const searchUserStart = () => ({
  type: actionTypes.SEARCH_USER_START,
});

export const searchUserSuccess = (searchuser) => ({
  type: actionTypes.SEARCH_USER_SUCCESS,
  payload: { searchuser },
});

export const searchUserFail = (error) => ({
  type: actionTypes.SEARCH_USER_FAIL,
  payload: { error },
});
