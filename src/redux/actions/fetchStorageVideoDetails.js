import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchStorageVideoDetails = (storageVideoId) => (dispatch) => {
  dispatch(fetchStorageVideoDetailsStart());
  const token = localStorage.jwtToken;

  UsersServices.fetchVideoDetails(token, storageVideoId)
    .then((res) => {
      //
      //
      console.log("res:", res);
      dispatch(fetchStorageVideoDetailsSuccess(res.data.data));
    })
    .catch((err) => {
      dispatch(fetchStorageVideoDetailsFail(err.response));
    });
};

export const fetchStorageVideoDetailsStart = () => ({
  type: actionTypes.FETCH_STORAGE_VIDEO_DETAILS_USER_START,
});

export const fetchStorageVideoDetailsSuccess = (storagevideodetails) => ({
  type: actionTypes.FETCH_STORAGE_VIDEO_DETAILS_USER_SUCCESS,
  payload: { storagevideodetails },
});

export const fetchStorageVideoDetailsFail = (error) => ({
  type: actionTypes.FETCH_STORAGE_VIDEO_DETAILS_USER_FAIL,
  payload: { error },
});
