import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchPublicPosts = () => (dispatch) => {
  UsersServices.fetchAllPostsPublic()
    .then((res) => {
      //
      //debugging
      // console.log("TEST", res.data.data);
      // console.log("res.status:", res);
      dispatch(fetchPublicPostsSuccess(res.data.data));
    })
    .catch((err) => {
      dispatch(fetchPublicPostsFail(err.response));
    });
};

export const fetchPublicPostsStart = () => ({
  type: actionTypes.FETCH_PUBLIC_POSTS_START,
});

export const fetchPublicPostsSuccess = (publicposts) => ({
  type: actionTypes.FETCH_PUBLIC_POSTS_SUCCESS,
  payload: { publicposts },
});

export const fetchPublicPostsFail = (error) => ({
  type: actionTypes.FETCH_PUBLIC_POSTS_FAIL,
  payload: { error },
});
