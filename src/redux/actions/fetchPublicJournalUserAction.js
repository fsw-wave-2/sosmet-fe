import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchPublicJournal = (journal_id) => async (dispatch) => {
  dispatch(fetchPublicJournalStart());
  await UsersServices.fetchPublicJournal(journal_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data.data);
      dispatch(fetchPublicJournalSuccess(res.data.data));
      document.title = ` ${res.data.data.title} • Sosmet`;
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(fetchPublicJournalFail(err.message));
    });
};

export const fetchPublicJournalStart = () => ({
  type: actionTypes.FETCH_PUBLIC_JOURNAL_USER_START,
});

export const fetchPublicJournalSuccess = (fetchpublicjournal) => ({
  type: actionTypes.FETCH_PUBLIC_JOURNAL_USER_SUCCESS,
  payload: { fetchpublicjournal },
});

export const fetchPublicJournalFail = (error) => ({
  type: actionTypes.FETCH_PUBLIC_JOURNAL_USER_FAIL,
  payload: { error },
});
