import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

//
//try function fetch - 1
// export const fetchPostsUserId = (user_id) => {
//   return async (dispatch) => {
//     dispatch(fetchPostsUserIdStart());
//     try {
//       const response = await UsersServices.latestPostUser(user_id);
//       const res = await handleErrors(response);
//       const data = await res.json();
//       //
//       //
//       console.log("response:", response);
//       console.log("data:", data.data);
//       dispatch(fetchPostsUserIdSuccess(data.data));
//     } catch (error) {
//       return dispatch(fetchPostsUserIdFail(error));
//     }
//   };
// };

//
//try function fetch - 2
export const fetchPostsUserId = (user_id) => (dispatch) => {
  dispatch(fetchPostsUserIdStart());
  UsersServices.latestPostUser(user_id)
    .then((res) => {
      //
      //debug
      console.log(res.data.data);
      dispatch(fetchPostsUserIdSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      console.log(err.message);
      dispatch(fetchPostsUserIdFail(err.message));
    });
};

// function handleErrors(response) {
//   if (!response.ok) {
//     throw Error(response.statusText);
//   }
//   return response;
// }

export const fetchPostsUserIdStart = () => ({
  type: actionTypes.FETCH_POSTS_USERID_START,
});

export const fetchPostsUserIdSuccess = (postsuserid) => ({
  type: actionTypes.FETCH_POSTS_USERID_SUCCESS,
  payload: { postsuserid },
});

export const fetchPostsUserIdFail = (error) => ({
  type: actionTypes.FETCH_POSTS_USERID_FAIL,
  payload: { error },
});
