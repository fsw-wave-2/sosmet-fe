import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchStorageAudios = (user_id) => (dispatch) => {
  const token = localStorage.jwtToken;
  UsersServices.fetchAudios(token, user_id)
    .then((res) => {
      //
      //debug
      // console.log("res.data:", res.data.data);
      dispatch(fetchAudiosSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      // console.log("err:", err.message);
      dispatch(fetchAudiosFail(err.message));
    });
};

export const fetchAudiosStart = () => ({
  type: actionTypes.FETCH_STORAGE_AUDIOS_USER_START,
});

export const fetchAudiosSuccess = (fetchstorageaudios) => ({
  type: actionTypes.FETCH_STORAGE_AUDIOS_USER_SUCCESS,
  payload: { fetchstorageaudios },
});

export const fetchAudiosFail = (error) => ({
  type: actionTypes.FETCH_STORAGE_AUDIOS_USER_FAIL,
  payload: { error },
});
