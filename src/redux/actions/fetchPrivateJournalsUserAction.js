import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchPrivateJournals = (user_id) => (dispatch) => {
  const token = localStorage.jwtToken;
  dispatch(fetchPrivateJournalsStart());
  UsersServices.fetchJournalsPrivate(token, user_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data.data);
      dispatch(fetchPrivateJournalsSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(fetchPrivateJournalsFail(err));
    });
};

export const fetchPrivateJournalsStart = () => ({
  type: actionTypes.FETCH_PRIVATE_JOURNALS_USER_START,
});

export const fetchPrivateJournalsSuccess = (fetchprivatejournals) => ({
  type: actionTypes.FETCH_PRIVATE_JOURNALS_USER_SUCCESS,
  payload: { fetchprivatejournals },
});

export const fetchPrivateJournalsFail = (error) => ({
  type: actionTypes.FETCH_PRIVATE_JOURNALS_USER_FAIL,
  payload: { error },
});
