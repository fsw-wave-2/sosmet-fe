import * as actionTypes from "./actionTypes";

export const registerModalShow = () => ({
  type: actionTypes.REGISTER_MODAL_SHOW,
});

export const registerModalHide = () => ({
  type: actionTypes.REGISTER_MODAL_HIDE,
});
