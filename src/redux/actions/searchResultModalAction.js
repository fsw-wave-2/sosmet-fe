import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchSearchUserOnNavbar = (username) => (dispatch) => {
  dispatch(searchResultModalStart(username));

  const token = localStorage.jwtToken;

  UsersServices.searchUserByUsername(username, token)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data);

      if (res.data.length == 1) {
        dispatch(searchResultModalShow(res.data[0]));
      } else {
        dispatch(searchResultModalError(res.data.message));
      }
    })
    .catch((err) => {
      //
      //debug
      console.log("err", err);
    });
};

export const searchResultModalStart = (searchpostnav) => ({
  type: actionTypes.SEARCH_RESULT_MODAL_START,
  payload: { searchpostnav },
});

export const searchResultModalShow = (searchresultnav) => ({
  type: actionTypes.SEARCH_RESULT_MODAL_SHOW,
  payload: { searchresultnav },
});

export const searchResultModalHide = () => ({
  type: actionTypes.SEARCH_RESULT_MODAL_HIDE,
});

export const searchResultModalError = (error) => ({
  type: actionTypes.SEARCH_RESULT_MODAL_ERROR,
  payload: { error },
});
