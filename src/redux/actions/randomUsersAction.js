import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchRandomUsers = () => (dispatch) => {
  dispatch(fetchRandomUsersStart());
  UsersServices.fetchRandomUsers()
    .then((res) => {
      //
      //debugging
      console.log("res.status:", res.data);
      dispatch(fetchRandomUsersSuccess(res.data));
    })
    .catch((err) => {
      dispatch(fetchRandomUsersFail(err.response));
    });
};

export const fetchRandomUsersStart = () => ({
  type: actionTypes.FETCH_RANDOM_USERS_START,
});

export const fetchRandomUsersSuccess = (randomusers) => ({
  type: actionTypes.FETCH_RANDOM_USERS_SUCCESS,
  payload: { randomusers },
});

export const fetchRandomUsersFail = (error) => ({
  type: actionTypes.FETCH_RANDOM_USERS_FAIL,
  payload: { error },
});
