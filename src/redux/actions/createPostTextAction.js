import usersService from "../../services/usersService";
import * as actionTypes from "./actionTypes";

export const createPostText = (postText) => (dispatch) => {
  // dispatch(postStart(postText));
  const token = localStorage.jwtToken;
  usersService
    .createPostText(token, postText)
    .then((res) => {
      //
      //debug
      //console.log("dispatch(onLoadPost(postText):\n", dispatch(onLoadPost()));
      //console.log("params_postTextData", postText);
      //console.log("response_createPostText:\n", res);
      dispatch(postSuccess(res.data));
    })
    .catch((err) => {
      dispatch({ type: actionTypes.GET_ERRORS, payload: err.response.data });
      //
      //debug
      //console.log("err_response_createPostText:\n", err.response.data);
    });
};

export const postOnChange = () => ({
  type: actionTypes.CREATE_USER_POST_ONCHANGE,
});

export const postStart = (postText) => ({
  type: actionTypes.CREATE_USER_POST_START,
  payload: { postText },
});

export const postSuccess = (postresponse) => ({
  type: actionTypes.CREATE_USER_COMMENT_SUCCESS,
  payload: { postresponse },
});

//console.log("onloadPOSTTTT:\n", postSuccess);
