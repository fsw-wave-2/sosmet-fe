import * as actionTypes from "./actionTypes";
import { baseUrl } from "../../services/httpService";

//fetch suggested
export const fetchPublicUsers = () => {
  return async (dispatch) => {
    dispatch(fetchPublicUsersStart());
    try {
      const response = await fetch(`${baseUrl}/all-profiles`);
      const res = await handleErrors(response);
      const json = await res.json();

      dispatch(fetchPublicUsersSuccess(json));
      return json;
    } catch (error) {
      console.log("error:", error);
      return dispatch(fetchPublicUsersFail(error));
    }
  };
};

function handleErrors(response) {
  if (!response.ok) {
    throw Error(response.statusText);
  }
  return response;
}

export const fetchPublicUsersStart = () => ({
  type: actionTypes.FETCH_PUBLIC_USERS_START,
});

export const fetchPublicUsersSuccess = (publicusers) => ({
  type: actionTypes.FETCH_PUBLIC_USERS_SUCCESS,
  payload: { publicusers },
});

export const fetchPublicUsersFail = (error) => ({
  type: actionTypes.FETCH_PUBLIC_USERS_FAIL,
  payload: { error },
});
