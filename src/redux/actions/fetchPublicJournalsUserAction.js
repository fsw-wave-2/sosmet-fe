import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchPublicJournals = (user_id) => (dispatch) => {
  dispatch(fetchPublicJournalsStart());
  UsersServices.fetchPublicJournalsUser(user_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data.data);
      dispatch(fetchPublicJournalsSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(fetchPublicJournalsFail(err));
    });
};

export const fetchPublicJournalsStart = () => ({
  type: actionTypes.FETCH_PUBLIC_JOURNALS_USER_START,
});

export const fetchPublicJournalsSuccess = (fetchpublicjournals) => ({
  type: actionTypes.FETCH_PUBLIC_JOURNALS_USER_SUCCESS,
  payload: { fetchpublicjournals },
});

export const fetchPublicJournalsFail = (error) => ({
  type: actionTypes.FETCH_PUBLIC_JOURNALS_USER_FAIL,
  payload: { error },
});
