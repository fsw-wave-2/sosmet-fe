import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchPrivateJournal = (journal_id) => async (dispatch) => {
  dispatch(fetchPrivateJournalStart());

  const token = localStorage.jwtToken;
  await UsersServices.fetchJournal(token, journal_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data.data);
      dispatch(fetchPrivateJournalSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(fetchPrivateJournalFail(err.message));
    });
};

export const fetchPrivateJournalStart = () => ({
  type: actionTypes.FETCH_PRIVATE_JOURNAL_USER_START,
});

export const fetchPrivateJournalSuccess = (fetchprivatejournal) => ({
  type: actionTypes.FETCH_PRIVATE_JOURNAL_USER_SUCCESS,
  payload: { fetchprivatejournal },
});

export const fetchPrivateJournalFail = (error) => ({
  type: actionTypes.FETCH_PRIVATE_JOURNAL_USER_FAIL,
  payload: { error },
});
