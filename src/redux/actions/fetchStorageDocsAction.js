import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const fetchStorageDocs = (user_id) => (dispatch) => {
  const token = localStorage.jwtToken;
  dispatch(fetchStorageDocsStart());
  UsersServices.fetchDocs(token, user_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data.data);
      dispatch(fetchStorageDocsSuccess(res.data.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(fetchStorageDocsFail(err));
    });
};

export const fetchStorageDocsStart = () => ({
  type: actionTypes.FETCH_STORAGE_DOCS_USER_START,
});

export const fetchStorageDocsSuccess = (fetchstoragedocs) => ({
  type: actionTypes.FETCH_STORAGE_DOCS_USER_SUCCESS,
  payload: { fetchstoragedocs },
});

export const fetchStorageDocsFail = (error) => ({
  type: actionTypes.FETCH_STORAGE_DOCS_USER_FAIL,
  payload: { error },
});
