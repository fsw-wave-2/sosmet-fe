import * as actionTypes from "./actionTypes";
import UsersServices from "../../services/usersService";

export const updatePrivateJournal = (updateprivatejournal, journal_id) => (
  dispatch
) => {
  dispatch(updatePrivateJournalStart(updateprivatejournal));
  const token = localStorage.jwtToken;
  //
  //debug
  console.log("journal_id:", journal_id);
  console.log("updateprivatejournal:", updateprivatejournal);
  UsersServices.updateJournal(token, updateprivatejournal, journal_id)
    .then((res) => {
      //
      //debug
      console.log("res:", res.data);
      dispatch(updatePrivateJournalSuccess(res.data));
    })
    .catch((err) => {
      //
      //debug
      console.log("err:", err);
      dispatch(updatePrivateJournalFail(err.message));
    });
};

export const updatePrivateJournalStart = (updateprivatejournal) => ({
  type: actionTypes.UPDATE_PRIVATE_JOURNAL_USER_START,
  payload: { updateprivatejournal },
});

export const updatePrivateJournalSuccess = (success) => ({
  type: actionTypes.UPDATE_PRIVATE_JOURNAL_USER_SUCCESS,
  payload: { success },
});

export const updatePrivateJournalFail = (error) => ({
  type: actionTypes.UPDATE_PRIVATE_JOURNAL_USER_FAIL,
  payload: { error },
});
