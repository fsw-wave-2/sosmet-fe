import {
  FETCH_RANDOM_USERS_START,
  FETCH_RANDOM_USERS_SUCCESS,
  FETCH_RANDOM_USERS_FAIL,
} from "../actions/actionTypes";

const initialState = {
  randomusers: [],
  loading: false,
  error: null,
};

export default function randomUsersReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_RANDOM_USERS_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_RANDOM_USERS_SUCCESS:
      return {
        ...state,
        loading: false,
        randomusers: action.payload.randomusers,
      };
    case FETCH_RANDOM_USERS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error.status,
        randomusers: "",
      };
    default:
      return state;
  }
}
