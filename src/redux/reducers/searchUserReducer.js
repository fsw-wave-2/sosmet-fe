import {
  SEARCH_USER_START,
  SEARCH_USER_SUCCESS,
  SEARCH_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  searchuser: [],
  loading: false,
  error: null,
};

export default function searchUserReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case SEARCH_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        searchuser: action.payload.searchuser,
      };
    case SEARCH_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        searchuser: [],
      };
    default:
      return state;
  }
}
