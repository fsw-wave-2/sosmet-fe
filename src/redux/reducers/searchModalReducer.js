import {
  SEARCH_RESULT_MODAL_START,
  SEARCH_RESULT_MODAL_SHOW,
  SEARCH_RESULT_MODAL_HIDE,
  SEARCH_RESULT_MODAL_ERROR,
} from "../actions/actionTypes";

const initialState = {
  searchpostnav: "",
  showResultModal: false,
  searchresultnav: "",
  loading: false,
  error: null,
};

export default function searchModalReducer(state = initialState, action) {
  switch (action.type) {
    case SEARCH_RESULT_MODAL_START:
      return {
        ...state,
        showResultModal: true,
        loading: true,
        searchpostnav: action.payload.searchpostnav,
        error: null,
      };
    case SEARCH_RESULT_MODAL_SHOW:
      return {
        ...state,
        showResultModal: true,
        loading: false,
        searchresultnav: action.payload.searchresultnav,
        error: null,
      };
    case SEARCH_RESULT_MODAL_HIDE:
      return {
        ...state,
        showResultModal: false,
        searchpostnav: null,
        searchresultnav: null,
        error: null,
      };
    case SEARCH_RESULT_MODAL_ERROR:
      return {
        ...state,
        searchresultnav: null,
        showResultModal: true,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}
