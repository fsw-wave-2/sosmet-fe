import {
  FETCH_STORAGE_VIDEO_DETAILS_USER_START,
  FETCH_STORAGE_VIDEO_DETAILS_USER_SUCCESS,
  FETCH_STORAGE_VIDEO_DETAILS_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  storagevideodetails: [],
  loading: false,
  error: null,
};

export default function postdetailsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STORAGE_VIDEO_DETAILS_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_STORAGE_VIDEO_DETAILS_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        storagevideodetails: action.payload.storagevideodetails,
      };
    case FETCH_STORAGE_VIDEO_DETAILS_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        storagevideodetails: [],
      };
    default:
      return state;
  }
}
