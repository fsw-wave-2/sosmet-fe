import {
  CREATE_USER_POST_ONCHANGE,
  CREATE_USER_POST_START,
  CREATE_USER_POST_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  postText: "",
  postresponse: "",
  loading: false,
};

// console.log("initialState.postText:\n", initialState.postText);
// console.log("initialState.postText:\n", initialState.isLoading);

export default function postTextReducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_USER_POST_ONCHANGE:
      return {
        ...state,
        loading: false,
      };
    case CREATE_USER_POST_START:
      return {
        ...state,
        postText: action.payload.postText,
        loading: true,
      };
    case CREATE_USER_POST_SUCCESS:
      return {
        ...state,
        loading: false,
        postresponse: action.payload.postresponse,
      };
    default:
      return state;
  }
}
