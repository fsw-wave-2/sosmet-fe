import { combineReducers } from "redux";
import authReducer from "./authReducer";
import errorReducer from "./errorReducer";
import postTextReducer from "./postTextReducer";
import publicusers from "./publicUsers";
import linksInfoWeb from "./footerReducer";
import publicposts from "./publicPosts";
import whoami from "./whoAmiReducer";
import postdetails from "./postDetailsReducer";
import loves from "./publicPostLovesReducer";
import createcomment from "./createCommentReducer";
import commentspost from "./fetchCommentsPostReducer";
import fetchvideos from "./fetchVideosReducer";
import fetchimages from "./fetchImagesReducer";
import searchuser from "./searchUserReducer";
import postsuserid from "./fetchPostsUserIdReducer";
import loginmodal from "./loginModalReducer";
import registermodal from "./registerModalReducer";
import searchresultnav from "./searchModalReducer";
import fetchprivatejournals from "./fetchPrivateJournalsUserReducer";
import fetchprivatejournal from "./fetchPrivateJournalReducer";
import fetchpublicjournals from "./fetchPublicJournalsUserReducer";
import fetchpublicjournal from "./fetchPublicJournalReducer";
import updateprivatejournal from "./updatePrivateJournalReducer";
import randomusers from "./randomUsersReducer";
import storagevideodetails from "./fetchStorageVideoDetails";
import fetchstoragedocs from "./fetchStorageDocsReducer";
import fetchstorageaudios from "./fetchStorageAudiosReducer";

export default combineReducers({
  auth: authReducer,
  errors: errorReducer,
  postText: postTextReducer,
  publicposts: publicposts,
  linksInfoWeb: linksInfoWeb,
  publicusers: publicusers,
  whoami: whoami,
  postdetails: postdetails,
  loves: loves,
  createcomment: createcomment,
  commentspost: commentspost,
  fetchvideos: fetchvideos,
  fetchimages: fetchimages,
  searchuser: searchuser,
  postsuserid: postsuserid,
  loginmodal: loginmodal,
  registermodal: registermodal,
  searchresultnav: searchresultnav,
  fetchprivatejournals: fetchprivatejournals,
  fetchprivatejournal: fetchprivatejournal,
  updateprivatejournal: updateprivatejournal,
  fetchpublicjournals: fetchpublicjournals,
  fetchpublicjournal: fetchpublicjournal,
  randomusers: randomusers,
  storagevideodetails: storagevideodetails,
  fetchstoragedocs: fetchstoragedocs,
  fetchstorageaudios: fetchstorageaudios,
});
