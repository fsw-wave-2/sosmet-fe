import {
  FETCH_STORAGE_AUDIOS_USER_START,
  FETCH_STORAGE_AUDIOS_USER_SUCCESS,
  FETCH_STORAGE_AUDIOS_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  fetchstorageaudios: [],
  loading: false,
  error: null,
};

export default function fetchVideosReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_STORAGE_AUDIOS_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_STORAGE_AUDIOS_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        fetchstorageaudios: action.payload.fetchstorageaudios,
      };
    case FETCH_STORAGE_AUDIOS_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        fetchstorageaudios: [],
      };
    default:
      return state;
  }
}
