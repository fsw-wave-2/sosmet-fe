import {
  FETCH_PRIVATE_JOURNAL_USER_START,
  FETCH_PRIVATE_JOURNAL_USER_SUCCESS,
  FETCH_PRIVATE_JOURNAL_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  fetchprivatejournal: "",
  loading: false,
  error: null,
};

export default function fetchPrivateJournalReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case FETCH_PRIVATE_JOURNAL_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PRIVATE_JOURNAL_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        fetchprivatejournal: action.payload.fetchprivatejournal,
      };
    case FETCH_PRIVATE_JOURNAL_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        fetchprivatejournal: "",
      };
    default:
      return state;
  }
}
