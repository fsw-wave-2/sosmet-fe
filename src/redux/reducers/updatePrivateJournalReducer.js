import {
  UPDATE_PRIVATE_JOURNAL_USER_FAIL,
  UPDATE_PRIVATE_JOURNAL_USER_START,
  UPDATE_PRIVATE_JOURNAL_USER_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  updateprivatejournal: "",
  success: "",
  loading: false,
  error: null,
};

export default function updatePrivateJournalReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case UPDATE_PRIVATE_JOURNAL_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
        updateprivatejournal: action.payload.updateprivatejournal,
      };
    case UPDATE_PRIVATE_JOURNAL_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        error: null,
        success: action.payload.success,
      };
    case UPDATE_PRIVATE_JOURNAL_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
      };
    default:
      return state;
  }
}
