import {
  FETCH_POSTS_USERID_START,
  FETCH_POSTS_USERID_SUCCESS,
  FETCH_POSTS_USERID_FAIL,
} from "../actions/actionTypes";

const initialState = {
  postsuserid: [],
  loading: true,
  error: null,
};

export default function fetchPostsUserIdReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_POSTS_USERID_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_POSTS_USERID_SUCCESS:
      return {
        ...state,
        loading: false,
        postsuserid: action.payload.postsuserid,
      };
    case FETCH_POSTS_USERID_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        postsuserid: [],
      };
    default:
      return state;
  }
}
