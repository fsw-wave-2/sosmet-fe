import {
  FETCH_PUBLIC_POSTS_FAIL,
  FETCH_PUBLIC_POSTS_START,
  FETCH_PUBLIC_POSTS_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  publicposts: [],
  loading: false,
  error: null,
};

export default function publicPostsReducer(state = initialState, action) {
  switch (action.type) {
    case FETCH_PUBLIC_POSTS_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PUBLIC_POSTS_SUCCESS:
      return {
        ...state,
        loading: false,
        publicposts: action.payload.publicposts,
      };
    case FETCH_PUBLIC_POSTS_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error.status,
        publicposts: [],
      };
    default:
      return state;
  }
}
