import {
  REGISTER_MODAL_SHOW,
  REGISTER_MODAL_HIDE,
} from "../actions/actionTypes";

const initialState = {
  showRegister: false,
};

export default function registerModalReducer(state = initialState, action) {
  switch (action.type) {
    case REGISTER_MODAL_SHOW:
      return {
        ...state,
        showRegister: true,
      };
    case REGISTER_MODAL_HIDE: {
      return {
        ...state,
        showRegister: false,
      };
    }
    default:
      return state;
  }
}
