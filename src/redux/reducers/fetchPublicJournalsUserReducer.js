import {
  FETCH_PUBLIC_JOURNALS_USER_FAIL,
  FETCH_PUBLIC_JOURNALS_USER_START,
  FETCH_PUBLIC_JOURNALS_USER_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  fetchpublicjournals: [],
  loading: false,
  error: null,
};

export default function fetchPublicJournalsReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case FETCH_PUBLIC_JOURNALS_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PUBLIC_JOURNALS_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        fetchpublicjournals: action.payload.fetchpublicjournals,
      };
    case FETCH_PUBLIC_JOURNALS_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        fetchpublicjournals: "",
      };
    default:
      return state;
  }
}
