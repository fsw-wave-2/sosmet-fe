import {
  FETCH_PRIVATE_JOURNALS_USER_FAIL,
  FETCH_PRIVATE_JOURNALS_USER_START,
  FETCH_PRIVATE_JOURNALS_USER_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
  fetchprivatejournals: "",
  loading: false,
  error: null,
};

export default function fetchPrivateJournalsReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case FETCH_PRIVATE_JOURNALS_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PRIVATE_JOURNALS_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        fetchprivatejournals: action.payload.fetchprivatejournals,
      };
    case FETCH_PRIVATE_JOURNALS_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        fetchprivatejournals: "",
      };
    default:
      return state;
  }
}
