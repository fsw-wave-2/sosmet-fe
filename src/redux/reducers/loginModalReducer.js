import { LOGIN_MODAL_SHOW, LOGIN_MODAL_HIDE } from "../actions/actionTypes";

const initialState = {
  showLogin: false,
};

export default function loginModalReducer(state = initialState, action) {
  switch (action.type) {
    case LOGIN_MODAL_SHOW:
      return {
        ...state,
        showLogin: true,
      };
    case LOGIN_MODAL_HIDE: {
      return {
        ...state,
        showLogin: false,
      };
    }
    default:
      return state;
  }
}
