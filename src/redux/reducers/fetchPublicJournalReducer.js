import {
  FETCH_PUBLIC_JOURNAL_USER_START,
  FETCH_PUBLIC_JOURNAL_USER_SUCCESS,
  FETCH_PUBLIC_JOURNAL_USER_FAIL,
} from "../actions/actionTypes";

const initialState = {
  fetchpublicjournal: "",
  loading: false,
  error: null,
};

export default function fetchPrivateJournalReducer(
  state = initialState,
  action
) {
  switch (action.type) {
    case FETCH_PUBLIC_JOURNAL_USER_START:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_PUBLIC_JOURNAL_USER_SUCCESS:
      return {
        ...state,
        loading: false,
        fetchpublicjournal: action.payload.fetchpublicjournal,
      };
    case FETCH_PUBLIC_JOURNAL_USER_FAIL:
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        fetchpublicjournal: "",
      };
    default:
      return state;
  }
}
