import { api } from "./httpService";

//
//debug
// console.log("url", baseUrl);
// console.log("api", api);

class UsersServices {
  //
  //data public
  fetchAllUsers() {
    return api.get("/all-profiles");
  }

  fetchRandomUsers() {
    return api.get("/random-users");
  }

  fetchAllPostsPublic() {
    return api.get("/posts");
  }

  fetchPostDetails(postId) {
    return api.get(`/post/${postId}`);
  }

  latestPostUser = (user_id) => {
    return api.get(`/posts/${user_id}`);
  };

  fetchLoveReceived = (postId) => {
    return api.get(`/total-love/${postId}`);
  };

  fetchCommentsPost = (postId) => {
    return api.get(`/comments/${postId}`);
  };

  fetchPublicJournalsUser = (user_id) => {
    return api.get(`/journals-publish-user/${user_id}`);
  };

  fetchPublicJournal = (journal_id) => {
    return api.get(`/journal-public/${journal_id}`);
  };
  //
  //
  //
  //
  //
  //data private
  whoami = (token) => {
    return api.get("/user/profile/me", { headers: { authorization: token } });
  };

  searchUserByUsername = (username, token) => {
    return api.get(`/user?username=${username}`, {
      headers: { authorization: token },
    });
  };

  updateWhoAmi = (user_id, whoami, token) => {
    return api.put(`/user/${user_id}`, whoami, {
      headers: { authorization: token },
    });
  };

  updateImageProfile = (user_id, token) => {
    return api.put(`/avatar/${user_id}`, {
      headers: { authorization: token },
    });
  };

  createPostText = (token, postTextData) => {
    return api.post("/new-post-text", postTextData, {
      headers: { authorization: token },
    });
  };

  createPostImage = (token, user_id, postImageData) => {
    return api.post(`/new-post-image/${user_id}`, postImageData, {
      headers: { authorization: token },
      "content-type": "multipart/form-data",
    });
  };

  giveLovePost = (token, loveData) => {
    return api.post("/love-post", loveData, {
      headers: { authorization: token },
    });
  };

  createComment = (token, comment) => {
    return api.post("/comment-post", comment, {
      headers: { authorization: token },
    });
  };

  uploadImages = (user_id, image_data, token) => {
    const image_append = new FormData();
    image_append.append("image", image_data);

    // debug;
    console.log("image_append", image_append);

    return api.post(`/upload-image/${user_id}`, image_append, {
      headers: { authorization: token },
    });
  };

  uploadVideos = (user_id, video_data, token) => {
    const video_append = new FormData();
    video_append.append("video", video_data);

    // debug;
    console.log("video_append", video_append);

    return api.post(`/upload-video/${user_id}`, video_append, {
      headers: { authorization: token },
    });
  };

  uploadAudios = (user_id, audio_data, token) => {
    const audio_append = new FormData();
    audio_append.append("audio", audio_data);

    // debug;
    console.log("audio_append", audio_append);

    return api.post(`/upload-audio/${user_id}`, audio_append, {
      headers: { authorization: token },
    });
  };

  uploadDocs = (user_id, doc_data, token) => {
    const doc_append = new FormData();
    doc_append.append("pdf", doc_data);

    // debug;
    console.log("doc_append", doc_append);

    return api.post(`/upload-doc/${user_id}`, doc_append, {
      headers: { authorization: token },
    });
  };

  fetchImages = (token, user_id) => {
    return api.get(`/storage-images/${user_id}`, {
      headers: { authorization: token },
    });
  };

  fetchVideos = (token, user_id) => {
    return api.get(`/storage-videos/${user_id}`, {
      headers: { authorization: token },
    });
  };

  fetchAudios = (token, user_id) => {
    return api.get(`/storage-audios/${user_id}`, {
      headers: { authorization: token },
    });
  };

  fetchDocs = (token, user_id) => {
    return api.get(`/storage-docs/${user_id}`, {
      headers: { authorization: token },
    });
  };

  fetchImageDetails = (token, storage_image_id) => {
    return api.get(`/storage-image-details/${storage_image_id}`, {
      headers: { authorization: token },
    });
  };

  fetchVideoDetails = (token, storage_video_id) => {
    return api.get(`/storage-video-details/${storage_video_id}`, {
      headers: { authorization: token },
    });
  };

  fetchAudioDetails = (token, storage_audio_id) => {
    return api.get(`/storage-audio-details/${storage_audio_id}`, {
      headers: { authorization: token },
    });
  };

  fetchDocDetails = (token, storage_doc_id) => {
    return api.get(`/storage-doc-details/${storage_doc_id}`, {
      headers: { authorization: token },
    });
  };

  createJournal = (token, journal_data) => {
    return api.post(`/journal-create`, journal_data, {
      headers: { authorization: token },
    });
  };

  fetchJournal = (token, journal_id) => {
    return api.get(`/journal/${journal_id}`, {
      headers: { authorization: token },
    });
  };

  fetchJournalsPrivate = (token, user_id) => {
    return api.get(`/journals-user/${user_id}`, {
      headers: { authorization: token },
    });
  };

  publishJournal = (token, set_public, journal_id) => {
    return api.put(`/journal-publish/${journal_id}`, set_public, {
      headers: { authorization: token },
    });
  };

  updateJournal = (token, updateaprivatejournal, journal_id) => {
    return api.put(`/journal-update/${journal_id}`, updateaprivatejournal, {
      headers: { authorization: token },
    });
  };
}

export default new UsersServices();
