import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import LoadingMask from 'react-loadingmask';

// Related to Authentication
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import { setCurrentUser, logoutUser } from "./redux/actions/authAction";

// Related to store
import store from "./redux/store/store";
import { Provider } from "react-redux";

//import component
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";
import PrivateRoute from "./components/private-route/PrivateRoute";
import Landing from "./pages/LandingOrHomePage";
import EditProfile from "./pages/EditProfilePage";
import SuggestedPage from "./pages/SuggestedAccountPage";
import PublicUserPage from "./pages/PublicUserPage";
import StoragePage from "./pages/StoragePage";
import PostDetails from "./pages/PostDetailPage";
import JournalsEditor from "./pages/journalsEditorPage";
import history from "./history";

//Importing stylesheet
// import "react-loadingmask/dist/react-loadingmask.css";
import "sanstrap/dist/css/sanstrap.css";
import "./index.css";

class App extends Component {
  render() {
    // check for token  to keep user login
    if (localStorage.jwtToken) {
      //set auth token header auth
      const token = localStorage.jwtToken;
      setAuthToken(token);
      // Decode token and get user info and exp token info
      const decoded = jwt_decode(token);

      // Set user and isAuthenticated
      store.dispatch(setCurrentUser(decoded));

      //Check for expired token
      const currentTime = Date.now() / 1000; // to get in milliseconds
      if (decoded.exp < currentTime) {
        //logout user
        store.dispatch(logoutUser());

        //Redirect to login
        window.location.href = "./";
      }
    }

    return (
      <Provider store={store}>
        <Router history={history}>
          <div className="App">
            <Switch>
              <Route exact path="/" component={Landing} />
              <Route path="/home" component={Landing} />
              <Route path="/accounts/login" component={Login} />
              <Route path="/accounts/register" component={Register} />
              <PrivateRoute path="/suggested" component={SuggestedPage} />
              <PrivateRoute path="/accounts/storage" component={StoragePage} />
              <PrivateRoute path="/accounts/edit" component={EditProfile} />
              <PrivateRoute
                path="/accounts/journal_editor"
                component={JournalsEditor}
              />
              <PrivateRoute path="/post/:id" component={PostDetails} />
              <PrivateRoute path="/:username" component={PublicUserPage} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
